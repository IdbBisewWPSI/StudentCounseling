CREATE VIEW distutionfee AS select university.universityid, degree.degree, tutionfee.tuitionFees, tutionfee.livingcost from tutionfee join university on university.universityid=tutionfee.universityID join degree on degree.degreeid=tutionfee.degreeID


CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `displaycity` AS select `city`.`cityid` AS `cityid`,`country`.`countryname` AS `countryname`,`city`.`cityname` AS `cityname` from (`city` join `country` on((`city`.`countryid` = `country`.`countryid`)));


CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `disucampus` AS select `universitycampus`.`slno` AS `slno`,`university`.`universityname` AS `universityname`,`universitycampus`.`imagetitle` AS `imagetitle`,`universitycampus`.`campusimage` AS `campusimage` from (`universitycampus` join `university` on((`universitycampus`.`universityid` = `university`.`universityid`)));


CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `disudegree` AS select `universitydegree`.`slno` AS `slno`,`university`.`universityname` AS `universityname`,`degree`.`degree` AS `degree`,`degree`.`degreeid` AS `degreeid`,`university`.`universityid` AS `universityid` from ((`universitydegree` join `university` on((`universitydegree`.`univercityid` = `university`.`universityid`))) join `degree` on((`universitydegree`.`degreeid` = `degree`.`degreeid`)));


CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `disuniversity` AS select `university`.`universityid` AS `universityid`,`university`.`logo` AS `logo`,`university`.`universityname` AS `universityname`,`country`.`countryname` AS `countryname`,`city`.`cityname` AS `cityname`,`university`.`aboutuniversity` AS `aboutuniversity`,`university`.`applicationdeadline` AS `applicationdeadline`,`university`.`email` AS `email`,`university`.`web` AS `web`,`university`.`fontpage` AS `fontpage`,`university`.`rank` AS `rank`,`university`.`rankimage` AS `rankimage` from ((`university` join `country` on((`university`.`countryid` = `country`.`countryid`))) join `city` on((`city`.`cityid` = `university`.`cityid`)));


CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `disusubject` AS select `universitysubject`.`slno` AS `slno`,`university`.`universityname` AS `universityname`,`subject`.`subjectname` AS `subjectname` from ((`universitysubject` join `university` on((`universitysubject`.`univercityid` = `university`.`universityid`))) join `subject` on((`universitysubject`.`subjectid` = `subject`.`subjectid`)));