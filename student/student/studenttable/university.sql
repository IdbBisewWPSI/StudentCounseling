-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Apr 29, 2015 at 08:51 PM
-- Server version: 5.6.20
-- PHP Version: 5.5.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `projectdb`
--

-- --------------------------------------------------------

--
-- Table structure for table `university`
--

CREATE TABLE IF NOT EXISTS `university` (
`universityid` int(11) NOT NULL,
  `logo` varchar(100) NOT NULL,
  `universityname` varchar(100) NOT NULL,
  `countryid` int(11) NOT NULL,
  `cityid` int(11) NOT NULL,
  `aboutuniversity` longtext,
  `applicationdeadline` date DEFAULT NULL,
  `applicationperiod` varchar(11) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `web` varchar(200) DEFAULT NULL,
  `fontpage` varchar(200) DEFAULT NULL,
  `rank` int(15) DEFAULT NULL,
  `rankimage` varchar(200) DEFAULT NULL,
  `mapurl` text NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=46 ;

--
-- Dumping data for table `university`
--

INSERT INTO `university` (`universityid`, `logo`, `universityname`, `countryid`, `cityid`, `aboutuniversity`, `applicationdeadline`, `applicationperiod`, `email`, `web`, `fontpage`, `rank`, `rankimage`, `mapurl`) VALUES
(44, '03102015202647oxfordlogo.png', 'University of Oxford', 8, 32, 'A university (Latin: universitas, "a whole") is an institution of higher (or tertiary) education and research which grants academic degrees in a variety of subjects ...?University of Bologna - ?Lists of universities', '2015-03-18', '6 Months', 'oxforduniversity@gmail.com', 'www.oxforduniversity.com', '03102015202647campus02.jpg', 5, '5', ''),
(45, '04202015221430logo.png', 'University of london', 9, 33, 'Santa Ana College, established in 1915, is one of Californias oldest community colleges. Santa Ana.', '2015-04-24', '6 months', 'sdfsadf@saf.ccc', 'www.bngls.com', '04202015221430campus01.jpg', 1, '4', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `university`
--
ALTER TABLE `university`
 ADD PRIMARY KEY (`universityid`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `university`
--
ALTER TABLE `university`
MODIFY `universityid` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=46;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
