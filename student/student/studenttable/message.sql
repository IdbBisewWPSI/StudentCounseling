-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Apr 29, 2015 at 07:54 PM
-- Server version: 5.6.20
-- PHP Version: 5.5.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `projectdb`
--

-- --------------------------------------------------------

--
-- Table structure for table `message`
--

CREATE TABLE IF NOT EXISTS `message` (
`id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `email` varchar(100) NOT NULL,
  `message` text NOT NULL,
  `txtdate` varchar(50) NOT NULL,
  `university` varchar(200) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `message`
--

INSERT INTO `message` (`id`, `name`, `email`, `message`, `txtdate`, `university`) VALUES
(5, 'hafiz', 'hafiz.ubikm@gmail.com', 'Bangladesh. Established during the British Raj in 1921, it gained a reputation as the "Oxford of the East" during its early years and has been a significant contributor to the modern history of Bangladesh.', '08/03/2015', 'Dhaka University'),
(6, 'Sumon', 'sumon@gmal.com', 'Bangladesh. Established during the British Raj in 1921, it gained a reputation as the "Oxford of the East" during its early years and has been a significant contributor to the modern history of Bangladesh.', '08/03/2015', 'Dhaka University'),
(7, 'manna', 'Manna@gmai.com', 'Bangladesh. Established during the British Raj in 1921, it gained a reputation as the "Oxford of the East" during its early years and has been a significant contributor to the modern history of Bangladesh.', '08/03/2015', 'Dhaka University'),
(8, 'Ashik', 'ashik@gmail.com', 'Bangladesh. Established during the British Raj in 1921, it gained a reputation as the "Oxford of the East" during its early years and has been a significant contributor to the modern history of Bangladesh.', '08/03/2015', 'Dhaka University'),
(9, 'Mamun', 'manun@gmail.com', 'Bangladesh. Established during the British Raj in 1921, it gained a reputation as the "Oxford of the East" during its early years and has been a significant contributor to the modern history of Bangladesh.', '08/03/2015', 'Dhaka University'),
(10, 'Joy', 'joy@gmail.com', 'Bangladesh. Established during the British Raj in 1921, it gained a reputation as the "Oxford of the East" during its early years and has been a significant contributor to the modern history of Bangladesh.', '08/03/2015', 'Dhaka University'),
(11, 'Delware Sumon', 'delware@gmail.com', 'Hello I want to learn in this ', '10/03/2015', 'University of Oxford'),
(12, 'Delware Sumon', 'delware@gmail.com', 'Hello I want to learn in this ', '10/03/2015', 'University of Oxford');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `message`
--
ALTER TABLE `message`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `message`
--
ALTER TABLE `message`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
