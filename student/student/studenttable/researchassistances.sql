-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Apr 29, 2015 at 08:51 PM
-- Server version: 5.6.20
-- PHP Version: 5.5.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `projectdb`
--

-- --------------------------------------------------------

--
-- Table structure for table `researchassistances`
--

CREATE TABLE IF NOT EXISTS `researchassistances` (
`researchassistancesID` int(11) NOT NULL,
  `universityID` int(11) NOT NULL,
  `degreeID` int(11) NOT NULL,
  `ResearchAssistances` text NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `researchassistances`
--

INSERT INTO `researchassistances` (`researchassistancesID`, `universityID`, `degreeID`, `ResearchAssistances`) VALUES
(1, 44, 6, '<ul><li><span style="line-height: 1.42857143;">fsafasf afas fsa saf as</span><br></li><li><span style="line-height: 1.42857143;">asfas sa asfsa fs&nbsp;</span><br></li><li><span style="line-height: 1.42857143;">as sfsf saf sf sfs fs</span><br></li><li><span style="line-height: 1.42857143;">fas &nbsp;sadfas fas fs as&nbsp;</span><br></li><li><span style="line-height: 1.42857143;">fsfasfa &nbsp;sf saf as a fas&nbsp;</span><br></li><li><span style="line-height: 1.42857143;">asfa sf asfsa sasa</span><br></li></ul>'),
(2, 44, 10, '<ol><li><span style="line-height: 1.42857143;">&#2447;&#2439;&#2463;&#2494; &#2453;&#2495;&#2458;&#2509;&#2459;&#2497; &#2476;&#2497;&#2461;&#2495; &#2472;&#2494;&#2439;</span><br></li><li><span style="line-height: 1.42857143;">&#2447;&#2439; &#2463;&#2503;&#2476;&#2495;&#2482;&#2503;&#2480; &#2453;&#2495; &#2453;&#2494;&#2460;?</span><br></li><li><span style="line-height: 1.42857143;">&#2468;&#2494;&#2451; &#2453;&#2480;&#2482;&#2494;&#2478; &#2453;&#2495;&#2472;&#2509;&#2468;&#2497; &#2447;&#2480; &#2465;&#2494;&#2463;&#2494; &#2453;&#2439; &#2486;&#2507; &#2453;&#2480;&#2476;</span><br></li><li><span style="line-height: 1.42857143;">&#2453;&#2503;&#2441; &#2438;&#2478;&#2494;&#2480;&#2503; &#2453;&#2439;&#2527;&#2494; &#2470;&#2503;&#2472;..!</span><br></li></ol>');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `researchassistances`
--
ALTER TABLE `researchassistances`
 ADD PRIMARY KEY (`researchassistancesID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `researchassistances`
--
ALTER TABLE `researchassistances`
MODIFY `researchassistancesID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
