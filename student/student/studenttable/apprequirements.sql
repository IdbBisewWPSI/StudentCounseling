-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Apr 29, 2015 at 07:52 PM
-- Server version: 5.6.20
-- PHP Version: 5.5.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `projectdb`
--

-- --------------------------------------------------------

--
-- Table structure for table `apprequirements`
--

CREATE TABLE IF NOT EXISTS `apprequirements` (
`appRequirementsID` int(11) NOT NULL,
  `universityID` int(11) NOT NULL,
  `degreeID` int(11) NOT NULL,
  `appRequirements` text NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `apprequirements`
--

INSERT INTO `apprequirements` (`appRequirementsID`, `universityID`, `degreeID`, `appRequirements`) VALUES
(1, 44, 5, '<ol><li><span style="line-height: 1.42857143;">Cirtifecate</span><br></li><li><span style="line-height: 1.42857143;">Birth Cirtifecate</span><br></li><li><span style="line-height: 1.42857143;">National ID Card</span><br></li></ol>'),
(2, 44, 9, '<ol><li><span style="line-height: 1.42857143;">Cirtifecate</span><br></li><li><span style="line-height: 1.42857143;">Birth Cirtifecate</span><br></li><li><span style="line-height: 1.42857143;">National ID Card</span><br></li></ol>'),
(3, 44, 9, '<ol><li><span style="line-height: 1.42857143;">requirments one</span><br></li><li><span style="line-height: 1.42857143;">requirments two</span><br></li><li><span style="line-height: 1.42857143;">requirments three</span><br></li><li><span style="line-height: 1.42857143;">requiremnts four</span><br></li></ol>');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `apprequirements`
--
ALTER TABLE `apprequirements`
 ADD PRIMARY KEY (`appRequirementsID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `apprequirements`
--
ALTER TABLE `apprequirements`
MODIFY `appRequirementsID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
