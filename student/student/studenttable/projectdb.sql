-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Apr 29, 2015 at 08:38 PM
-- Server version: 5.6.20
-- PHP Version: 5.5.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `projectdb`
--

-- --------------------------------------------------------

--
-- Table structure for table `accommodation`
--

CREATE TABLE IF NOT EXISTS `accommodation` (
`accommodationID` int(11) NOT NULL,
  `universityID` int(11) NOT NULL,
  `degreeID` int(11) NOT NULL,
  `accommodation` text NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `accommodation`
--

INSERT INTO `accommodation` (`accommodationID`, `universityID`, `degreeID`, `accommodation`) VALUES
(1, 44, 9, '<font color="#545454" face="arial, sans-serif" size="2"><span style="line-height: 18.2000007629395px;">408/C Free School Street Road, Hatirpool, Dhanmondi, Dhaka, Bangladesh.</span></font><br>');

-- --------------------------------------------------------

--
-- Table structure for table `application`
--

CREATE TABLE IF NOT EXISTS `application` (
`applicationid` int(11) NOT NULL,
  `fname` varchar(50) NOT NULL,
  `lname` varchar(50) NOT NULL,
  `countryid` int(11) NOT NULL,
  `locationid` int(11) NOT NULL,
  `state` varchar(50) NOT NULL,
  `postalcode` varchar(50) NOT NULL,
  `postaladdress` text NOT NULL,
  `phone` varchar(50) NOT NULL,
  `mobile` varchar(50) NOT NULL,
  `dob` varchar(30) NOT NULL,
  `gender` varchar(10) NOT NULL,
  `beginstudy` varchar(50) NOT NULL,
  `likedstudy` varchar(50) NOT NULL,
  `tuitionfee` varchar(20) NOT NULL,
  `test` varchar(20) NOT NULL,
  `score` varchar(30) NOT NULL,
  `appdate` varchar(30) NOT NULL,
  `comments` text NOT NULL,
  `email` varchar(200) NOT NULL,
  `status` varchar(20) NOT NULL
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `application`
--

INSERT INTO `application` (`applicationid`, `fname`, `lname`, `countryid`, `locationid`, `state`, `postalcode`, `postaladdress`, `phone`, `mobile`, `dob`, `gender`, `beginstudy`, `likedstudy`, `tuitionfee`, `test`, `score`, `appdate`, `comments`, `email`, `status`) VALUES
(1, 'Hafizur', 'Rahman', 0, 0, 'Dhaka', '1205', 'Dhanmondi', '6366546', '464646', '2015-04-15', '1', '2015-04-17', 'hafiz.ubikm@gmail.com', '10,000 - 15,000', 'ITEP', '5', '2015-04-15', 'sfsad sdfas fas asfas fasfa asfafa fasfas', 'hafiz.ubikm@gmail.com', 'Progress'),
(7, 'Delware', 'Sumon', 0, 0, 'Dhaka', '1205', 'Dhanmondi', '6366546', '01926743629', '2015-04-22', 'Male', '2015-04-20', 'hafiz.ubikm@gmail.com', '10,000 - 15,000', 'Pearson', '5', '2015-04-21', 'Other Comments or Questions in English', 'hafiz.ubikm@gmail.com', 'Active');

-- --------------------------------------------------------

--
-- Table structure for table `apprequirements`
--

CREATE TABLE IF NOT EXISTS `apprequirements` (
`appRequirementsID` int(11) NOT NULL,
  `universityID` int(11) NOT NULL,
  `degreeID` int(11) NOT NULL,
  `appRequirements` text NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `apprequirements`
--

INSERT INTO `apprequirements` (`appRequirementsID`, `universityID`, `degreeID`, `appRequirements`) VALUES
(1, 44, 5, '<ol><li><span style="line-height: 1.42857143;">Cirtifecate</span><br></li><li><span style="line-height: 1.42857143;">Birth Cirtifecate</span><br></li><li><span style="line-height: 1.42857143;">National ID Card</span><br></li></ol>'),
(2, 44, 9, '<ol><li><span style="line-height: 1.42857143;">Cirtifecate</span><br></li><li><span style="line-height: 1.42857143;">Birth Cirtifecate</span><br></li><li><span style="line-height: 1.42857143;">National ID Card</span><br></li></ol>'),
(3, 44, 9, '<ol><li><span style="line-height: 1.42857143;">requirments one</span><br></li><li><span style="line-height: 1.42857143;">requirments two</span><br></li><li><span style="line-height: 1.42857143;">requirments three</span><br></li><li><span style="line-height: 1.42857143;">requiremnts four</span><br></li></ol>');

-- --------------------------------------------------------

--
-- Table structure for table `city`
--

CREATE TABLE IF NOT EXISTS `city` (
`cityid` int(11) NOT NULL,
  `countryid` int(11) NOT NULL,
  `cityname` varchar(100) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=36 ;

--
-- Dumping data for table `city`
--

INSERT INTO `city` (`cityid`, `countryid`, `cityname`) VALUES
(32, 8, 'Wellington Square'),
(33, 9, 'Dhaka'),
(34, 9, 'Khulna'),
(35, 9, 'Rongp''r');

-- --------------------------------------------------------

--
-- Table structure for table `country`
--

CREATE TABLE IF NOT EXISTS `country` (
`countryid` int(11) NOT NULL,
  `countryname` varchar(50) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `country`
--

INSERT INTO `country` (`countryid`, `countryname`) VALUES
(8, 'England'),
(9, 'Bangladesh');

--
-- Triggers `country`
--
DELIMITER //
CREATE TRIGGER `delcountry` AFTER DELETE ON `country`
 FOR EACH ROW BEGIN
DELETE FROM city WHERE city.countryid = old.countryid;
END
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `degree`
--

CREATE TABLE IF NOT EXISTS `degree` (
`degreeid` int(11) NOT NULL,
  `degree` varchar(100) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `degree`
--

INSERT INTO `degree` (`degreeid`, `degree`) VALUES
(5, 'MSc'),
(6, 'DPhil'),
(8, 'PGDip'),
(9, 'EPSRC '),
(10, 'MFA'),
(11, 'PGCert');

-- --------------------------------------------------------

--
-- Table structure for table `degreecontent`
--

CREATE TABLE IF NOT EXISTS `degreecontent` (
`slno` int(11) NOT NULL,
  `universityID` int(11) NOT NULL,
  `degreeID` int(11) NOT NULL,
  `degreeContent` text NOT NULL
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `degreecontent`
--

INSERT INTO `degreecontent` (`slno`, `universityID`, `degreeID`, `degreeContent`) VALUES
(1, 44, 5, 'The University of Minnesota has been offering English as a Second Language courses and programs since 1968. Since then, we have helped thousands of students from across the globe achieve academic success, advance their careers, and develop international networks of friends.'),
(2, 44, 9, 'The University of Minnesota has been offering English as a Second Language courses and programs since 1968. Since then, we have helped .'),
(3, 45, 10, 'The mysqli_num_rows() function returns the number of rows in a The mysqli_num_rows() function returns the number of rows in a result set. result set. The mysqli_num_rows() function returns the number of rows in a result set. The University of Minnesota has been offering English as a Second Language courses and programs since 1968. Since then, we have helped thousands of students from across the globe achieve academic success, advance their careers, and develop international networks of friends.'),
(4, 45, 10, 'The University of Minnesota has been offering English as a Second Language courses and programs since 1968. Since then, we have helped thousands of students from across the globThe University of Minnesota has been offering English as a Second Language courses and programs since 1968. Since then, we have helped thousands of students from across the globe achieve academic success, advance their careers, and develop international networks of friends.');

-- --------------------------------------------------------

--
-- Stand-in structure for view `displaycity`
--
CREATE TABLE IF NOT EXISTS `displaycity` (
`cityid` int(11)
,`countryname` varchar(50)
,`cityname` varchar(100)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `displayinformation`
--
CREATE TABLE IF NOT EXISTS `displayinformation` (
`universityid` int(11)
,`logo` varchar(100)
,`universityname` varchar(100)
,`countryname` varchar(50)
,`cityname` varchar(100)
,`aboutuniversity` longtext
,`applicationdeadline` date
,`applicationperiod` varchar(11)
,`email` varchar(100)
,`web` varchar(200)
,`fontpage` varchar(200)
,`rank` int(15)
,`rankimage` varchar(200)
,`imagetitle` varchar(200)
,`campusimage` varchar(50)
,`degree` varchar(100)
,`subjectname` varchar(100)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `disucampus`
--
CREATE TABLE IF NOT EXISTS `disucampus` (
`slno` int(11)
,`universityname` varchar(100)
,`imagetitle` varchar(200)
,`campusimage` varchar(50)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `disudegree`
--
CREATE TABLE IF NOT EXISTS `disudegree` (
`slno` int(11)
,`universityname` varchar(100)
,`degree` varchar(100)
,`degreeid` int(11)
,`universityid` int(11)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `disuniversity`
--
CREATE TABLE IF NOT EXISTS `disuniversity` (
`universityid` int(11)
,`logo` varchar(100)
,`universityname` varchar(100)
,`countryname` varchar(50)
,`cityname` varchar(100)
,`aboutuniversity` longtext
,`applicationdeadline` date
,`email` varchar(100)
,`web` varchar(200)
,`fontpage` varchar(200)
,`rank` int(15)
,`rankimage` varchar(200)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `disusubject`
--
CREATE TABLE IF NOT EXISTS `disusubject` (
`slno` int(11)
,`universityname` varchar(100)
,`subjectname` varchar(100)
);
-- --------------------------------------------------------

--
-- Table structure for table `jobopportunities`
--

CREATE TABLE IF NOT EXISTS `jobopportunities` (
`jobOpportunitiesID` int(11) NOT NULL,
  `universityID` int(11) NOT NULL,
  `degreeID` int(11) NOT NULL,
  `jobOpportunities` text NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `jobopportunities`
--

INSERT INTO `jobopportunities` (`jobOpportunitiesID`, `universityID`, `degreeID`, `jobOpportunities`) VALUES
(1, 44, 10, '<ol><li><span style="line-height: 1.42857143;">Computer Engneer</span><br></li><li><span style="line-height: 1.42857143;">Mobile Engneer</span><br></li><li><span style="line-height: 1.42857143;">Web Developer</span><br></li></ol>');

-- --------------------------------------------------------

--
-- Table structure for table `message`
--

CREATE TABLE IF NOT EXISTS `message` (
`id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `email` varchar(100) NOT NULL,
  `message` text NOT NULL,
  `txtdate` varchar(50) NOT NULL,
  `university` varchar(200) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `message`
--

INSERT INTO `message` (`id`, `name`, `email`, `message`, `txtdate`, `university`) VALUES
(5, 'hafiz', 'hafiz.ubikm@gmail.com', 'Bangladesh. Established during the British Raj in 1921, it gained a reputation as the "Oxford of the East" during its early years and has been a significant contributor to the modern history of Bangladesh.', '08/03/2015', 'Dhaka University'),
(6, 'Sumon', 'sumon@gmal.com', 'Bangladesh. Established during the British Raj in 1921, it gained a reputation as the "Oxford of the East" during its early years and has been a significant contributor to the modern history of Bangladesh.', '08/03/2015', 'Dhaka University'),
(7, 'manna', 'Manna@gmai.com', 'Bangladesh. Established during the British Raj in 1921, it gained a reputation as the "Oxford of the East" during its early years and has been a significant contributor to the modern history of Bangladesh.', '08/03/2015', 'Dhaka University'),
(8, 'Ashik', 'ashik@gmail.com', 'Bangladesh. Established during the British Raj in 1921, it gained a reputation as the "Oxford of the East" during its early years and has been a significant contributor to the modern history of Bangladesh.', '08/03/2015', 'Dhaka University'),
(9, 'Mamun', 'manun@gmail.com', 'Bangladesh. Established during the British Raj in 1921, it gained a reputation as the "Oxford of the East" during its early years and has been a significant contributor to the modern history of Bangladesh.', '08/03/2015', 'Dhaka University'),
(10, 'Joy', 'joy@gmail.com', 'Bangladesh. Established during the British Raj in 1921, it gained a reputation as the "Oxford of the East" during its early years and has been a significant contributor to the modern history of Bangladesh.', '08/03/2015', 'Dhaka University'),
(11, 'Delware Sumon', 'delware@gmail.com', 'Hello I want to learn in this ', '10/03/2015', 'University of Oxford'),
(12, 'Delware Sumon', 'delware@gmail.com', 'Hello I want to learn in this ', '10/03/2015', 'University of Oxford');

-- --------------------------------------------------------

--
-- Table structure for table `researchassistances`
--

CREATE TABLE IF NOT EXISTS `researchassistances` (
`researchassistancesID` int(11) NOT NULL,
  `universityID` int(11) NOT NULL,
  `degreeID` int(11) NOT NULL,
  `ResearchAssistances` text NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `researchassistances`
--

INSERT INTO `researchassistances` (`researchassistancesID`, `universityID`, `degreeID`, `ResearchAssistances`) VALUES
(1, 44, 6, '<ul><li><span style="line-height: 1.42857143;">fsafasf afas fsa saf as</span><br></li><li><span style="line-height: 1.42857143;">asfas sa asfsa fs&nbsp;</span><br></li><li><span style="line-height: 1.42857143;">as sfsf saf sf sfs fs</span><br></li><li><span style="line-height: 1.42857143;">fas &nbsp;sadfas fas fs as&nbsp;</span><br></li><li><span style="line-height: 1.42857143;">fsfasfa &nbsp;sf saf as a fas&nbsp;</span><br></li><li><span style="line-height: 1.42857143;">asfa sf asfsa sasa</span><br></li></ul>'),
(2, 44, 10, '<ol><li><span style="line-height: 1.42857143;">&#2447;&#2439;&#2463;&#2494; &#2453;&#2495;&#2458;&#2509;&#2459;&#2497; &#2476;&#2497;&#2461;&#2495; &#2472;&#2494;&#2439;</span><br></li><li><span style="line-height: 1.42857143;">&#2447;&#2439; &#2463;&#2503;&#2476;&#2495;&#2482;&#2503;&#2480; &#2453;&#2495; &#2453;&#2494;&#2460;?</span><br></li><li><span style="line-height: 1.42857143;">&#2468;&#2494;&#2451; &#2453;&#2480;&#2482;&#2494;&#2478; &#2453;&#2495;&#2472;&#2509;&#2468;&#2497; &#2447;&#2480; &#2465;&#2494;&#2463;&#2494; &#2453;&#2439; &#2486;&#2507; &#2453;&#2480;&#2476;</span><br></li><li><span style="line-height: 1.42857143;">&#2453;&#2503;&#2441; &#2438;&#2478;&#2494;&#2480;&#2503; &#2453;&#2439;&#2527;&#2494; &#2470;&#2503;&#2472;..!</span><br></li></ol>');

-- --------------------------------------------------------

--
-- Table structure for table `scholarshipfee`
--

CREATE TABLE IF NOT EXISTS `scholarshipfee` (
`scholarshipFeeID` int(11) NOT NULL,
  `universityID` int(11) NOT NULL,
  `degreeID` int(11) NOT NULL,
  `scholarshipFee` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `subject`
--

CREATE TABLE IF NOT EXISTS `subject` (
`subjectid` int(11) NOT NULL,
  `subjectname` varchar(100) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `subject`
--

INSERT INTO `subject` (`subjectid`, `subjectname`) VALUES
(4, 'African Studies'),
(5, 'Ancient History'),
(6, 'Applied Statistics'),
(7, 'Islamic Studies and History'),
(8, 'Health Research'),
(9, 'Foundations of Computer Science'),
(10, 'Computer Science'),
(11, 'Major Programme Management');

-- --------------------------------------------------------

--
-- Table structure for table `tutionfee`
--

CREATE TABLE IF NOT EXISTS `tutionfee` (
`tuitionFeeID` int(11) NOT NULL,
  `universityID` int(11) NOT NULL,
  `degreeID` int(11) NOT NULL,
  `tuitionFees` text NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `tutionfee`
--

INSERT INTO `tutionfee` (`tuitionFeeID`, `universityID`, `degreeID`, `tuitionFees`) VALUES
(1, 44, 3, '<ol><li><span style="line-height: 1.42857143;">Hostel 5000/=</span><br></li><li><span style="line-height: 1.42857143;">Dining 2000/=</span><br></li><li><span style="line-height: 1.42857143;">Londry 5000/=</span><br></li><li><span style="line-height: 1.42857143;">Mobile 6566/=</span><br></li><li><span style="line-height: 1.42857143;">Web design 2100/=</span><br></li></ol>'),
(2, 44, 2, '<ol><li><span style="line-height: 1.42857143;">Hostel 5000/=</span><br></li><li><span style="line-height: 1.42857143;">Dining 2000/=</span><br></li><li><span style="line-height: 1.42857143;">Londry 5000/=</span><br></li><li><span style="line-height: 1.42857143;">Mobile 6566/=</span><br></li><li><span style="line-height: 1.42857143;">Web design 2100/=</span><br></li></ol>'),
(3, 44, 2, '<ol><li><span style="line-height: 1.42857143;">Hostel 5000/=</span><br></li><li><span style="line-height: 1.42857143;">Dining 2000/=</span><br></li><li><span style="line-height: 1.42857143;">Londry 5000/=</span><br></li><li><span style="line-height: 1.42857143;">Mobile 6566/=</span><br></li><li><span style="line-height: 1.42857143;">Web design 2100/=</span><br></li></ol>'),
(4, 44, 3, '<ul><li><b style="color: rgb(255, 0, 0); line-height: 1.42857143;"><i style="color:rgb(255,0,0);"><u style="color:rgb(255,0,0);">Bangladesh</u></i></b><br></li><li><b style="color: rgb(255, 0, 0); line-height: 1.42857143;"><i style="color:rgb(255,0,0);"><u style="color:rgb(255,0,0);"><p style="color: rgb(255, 0, 0); display: inline !important;">India</p></u></i></b><br></li><li><b style="color: rgb(255, 0, 0); line-height: 1.42857143;"><i style="color:rgb(255,0,0);"><u style="color:rgb(255,0,0);"><p style="color: rgb(255, 0, 0); display: inline !important;">Pakistan</p></u></i></b><br></li><li><b style="color: rgb(255, 0, 0); line-height: 1.42857143;"><i style="color:rgb(255,0,0);"><u style="color:rgb(255,0,0);"><p style="color: rgb(255, 0, 0); display: inline !important;">butan</p></u></i></b><br></li></ul>'),
(5, 44, 2, '<h5><ul><li><span style="line-height: 1.1;">University of dhaka</span><br></li><li><span style="font-size: 14px; line-height: 1.42857143;">alskfjas</span><br></li><li><span style="font-size: 14px; line-height: 1.42857143;">asflkasjfl</span><br></li><li><span style="font-size: 14px; line-height: 1.42857143;">asdflasfjas</span><br></li><li><span style="font-size: 14px; line-height: 1.42857143;">as</span><br></li><li><span style="font-size: 14px; line-height: 1.42857143;">fasjflkasjf</span><br></li><li><span style="font-size: 14px; line-height: 1.42857143;">asfaslkfjasasfasff</span><br></li><li><span style="font-size: 14px; line-height: 1.42857143;">adflsafhlskdf</span><br></li></ul></h5>');

-- --------------------------------------------------------

--
-- Table structure for table `tutionfeerange`
--

CREATE TABLE IF NOT EXISTS `tutionfeerange` (
`id` int(11) NOT NULL,
  `tutionfeerange` varchar(30) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `tutionfeerange`
--

INSERT INTO `tutionfeerange` (`id`, `tutionfeerange`) VALUES
(1, '100000-300000'),
(2, '300000-500000'),
(3, '500000-700000'),
(4, '700000-900000');

-- --------------------------------------------------------

--
-- Table structure for table `university`
--

CREATE TABLE IF NOT EXISTS `university` (
`universityid` int(11) NOT NULL,
  `logo` varchar(100) NOT NULL,
  `universityname` varchar(100) NOT NULL,
  `countryid` int(11) NOT NULL,
  `cityid` int(11) NOT NULL,
  `aboutuniversity` longtext,
  `applicationdeadline` date DEFAULT NULL,
  `applicationperiod` varchar(11) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `web` varchar(200) DEFAULT NULL,
  `fontpage` varchar(200) DEFAULT NULL,
  `rank` int(15) DEFAULT NULL,
  `rankimage` varchar(200) DEFAULT NULL,
  `mapurl` text NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=46 ;

--
-- Dumping data for table `university`
--

INSERT INTO `university` (`universityid`, `logo`, `universityname`, `countryid`, `cityid`, `aboutuniversity`, `applicationdeadline`, `applicationperiod`, `email`, `web`, `fontpage`, `rank`, `rankimage`, `mapurl`) VALUES
(44, '03102015202647oxfordlogo.png', 'University of Oxford', 8, 32, 'A university (Latin: universitas, "a whole") is an institution of higher (or tertiary) education and research which grants academic degrees in a variety of subjects ...?University of Bologna - ?Lists of universities', '2015-03-18', '6 Months', 'oxforduniversity@gmail.com', 'www.oxforduniversity.com', '03102015202647campus02.jpg', 5, '5', ''),
(45, '04202015221430logo.png', 'University of london', 9, 33, 'Santa Ana College, established in 1915, is one of Californias oldest community colleges. Santa Ana.', '2015-04-24', '6 months', 'sdfsadf@saf.ccc', 'www.bngls.com', '04202015221430campus01.jpg', 1, '4', '');

-- --------------------------------------------------------

--
-- Table structure for table `universitycampus`
--

CREATE TABLE IF NOT EXISTS `universitycampus` (
`slno` int(11) NOT NULL,
  `universityid` int(11) NOT NULL,
  `imagetitle` varchar(200) NOT NULL,
  `campusimage` varchar(50) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `universitycampus`
--

INSERT INTO `universitycampus` (`slno`, `universityid`, `imagetitle`, `campusimage`) VALUES
(9, 44, 'Oxford Brookes University', 'campus01.jpg'),
(10, 44, 'City of Oxford College', 'campus03.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `universitydegree`
--

CREATE TABLE IF NOT EXISTS `universitydegree` (
`slno` int(11) NOT NULL,
  `univercityid` int(11) NOT NULL,
  `degreeid` int(11) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `universitydegree`
--

INSERT INTO `universitydegree` (`slno`, `univercityid`, `degreeid`) VALUES
(1, 44, 5),
(2, 44, 6),
(3, 44, 7),
(4, 44, 8),
(5, 44, 9),
(6, 44, 10),
(7, 44, 11);

-- --------------------------------------------------------

--
-- Table structure for table `universitysubject`
--

CREATE TABLE IF NOT EXISTS `universitysubject` (
`slno` int(11) NOT NULL,
  `univercityid` int(11) NOT NULL,
  `subjectid` int(11) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `universitysubject`
--

INSERT INTO `universitysubject` (`slno`, `univercityid`, `subjectid`) VALUES
(6, 44, 4),
(7, 44, 5),
(8, 44, 6),
(9, 44, 7),
(10, 44, 8);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
`id` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `password`) VALUES
(8, 'hafiz', '5word'),
(9, 'sumon', '123456'),
(10, 'fouzia', '123456');

-- --------------------------------------------------------

--
-- Structure for view `displaycity`
--
DROP TABLE IF EXISTS `displaycity`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `displaycity` AS select `city`.`cityid` AS `cityid`,`country`.`countryname` AS `countryname`,`city`.`cityname` AS `cityname` from (`city` join `country` on((`city`.`countryid` = `country`.`countryid`)));

-- --------------------------------------------------------

--
-- Structure for view `displayinformation`
--
DROP TABLE IF EXISTS `displayinformation`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `displayinformation` AS select `university`.`universityid` AS `universityid`,`university`.`logo` AS `logo`,`university`.`universityname` AS `universityname`,`country`.`countryname` AS `countryname`,`city`.`cityname` AS `cityname`,`university`.`aboutuniversity` AS `aboutuniversity`,`university`.`applicationdeadline` AS `applicationdeadline`,`university`.`applicationperiod` AS `applicationperiod`,`university`.`email` AS `email`,`university`.`web` AS `web`,`university`.`fontpage` AS `fontpage`,`university`.`rank` AS `rank`,`university`.`rankimage` AS `rankimage`,`universitycampus`.`imagetitle` AS `imagetitle`,`universitycampus`.`campusimage` AS `campusimage`,`disudegree`.`degree` AS `degree`,`disusubject`.`subjectname` AS `subjectname` from (((((`university` join `country` on((`university`.`countryid` = `country`.`countryid`))) join `city` on((`university`.`cityid` = `city`.`cityid`))) join `universitycampus` on((`university`.`universityid` = `universitycampus`.`universityid`))) join `disudegree` on((`university`.`universityname` = `disudegree`.`universityname`))) join `disusubject` on((`university`.`universityname` = `disusubject`.`universityname`)));

-- --------------------------------------------------------

--
-- Structure for view `disucampus`
--
DROP TABLE IF EXISTS `disucampus`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `disucampus` AS select `universitycampus`.`slno` AS `slno`,`university`.`universityname` AS `universityname`,`universitycampus`.`imagetitle` AS `imagetitle`,`universitycampus`.`campusimage` AS `campusimage` from (`universitycampus` join `university` on((`universitycampus`.`universityid` = `university`.`universityid`)));

-- --------------------------------------------------------

--
-- Structure for view `disudegree`
--
DROP TABLE IF EXISTS `disudegree`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `disudegree` AS select `universitydegree`.`slno` AS `slno`,`university`.`universityname` AS `universityname`,`degree`.`degree` AS `degree`,`degree`.`degreeid` AS `degreeid`,`university`.`universityid` AS `universityid` from ((`universitydegree` join `university` on((`universitydegree`.`univercityid` = `university`.`universityid`))) join `degree` on((`universitydegree`.`degreeid` = `degree`.`degreeid`)));

-- --------------------------------------------------------

--
-- Structure for view `disuniversity`
--
DROP TABLE IF EXISTS `disuniversity`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `disuniversity` AS select `university`.`universityid` AS `universityid`,`university`.`logo` AS `logo`,`university`.`universityname` AS `universityname`,`country`.`countryname` AS `countryname`,`city`.`cityname` AS `cityname`,`university`.`aboutuniversity` AS `aboutuniversity`,`university`.`applicationdeadline` AS `applicationdeadline`,`university`.`email` AS `email`,`university`.`web` AS `web`,`university`.`fontpage` AS `fontpage`,`university`.`rank` AS `rank`,`university`.`rankimage` AS `rankimage` from ((`university` join `country` on((`university`.`countryid` = `country`.`countryid`))) join `city` on((`city`.`cityid` = `university`.`cityid`)));

-- --------------------------------------------------------

--
-- Structure for view `disusubject`
--
DROP TABLE IF EXISTS `disusubject`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `disusubject` AS select `universitysubject`.`slno` AS `slno`,`university`.`universityname` AS `universityname`,`subject`.`subjectname` AS `subjectname` from ((`universitysubject` join `university` on((`universitysubject`.`univercityid` = `university`.`universityid`))) join `subject` on((`universitysubject`.`subjectid` = `subject`.`subjectid`)));

--
-- Indexes for dumped tables
--

--
-- Indexes for table `accommodation`
--
ALTER TABLE `accommodation`
 ADD PRIMARY KEY (`accommodationID`);

--
-- Indexes for table `application`
--
ALTER TABLE `application`
 ADD PRIMARY KEY (`applicationid`);

--
-- Indexes for table `apprequirements`
--
ALTER TABLE `apprequirements`
 ADD PRIMARY KEY (`appRequirementsID`);

--
-- Indexes for table `city`
--
ALTER TABLE `city`
 ADD PRIMARY KEY (`cityid`);

--
-- Indexes for table `country`
--
ALTER TABLE `country`
 ADD PRIMARY KEY (`countryid`);

--
-- Indexes for table `degree`
--
ALTER TABLE `degree`
 ADD PRIMARY KEY (`degreeid`);

--
-- Indexes for table `degreecontent`
--
ALTER TABLE `degreecontent`
 ADD PRIMARY KEY (`slno`);

--
-- Indexes for table `jobopportunities`
--
ALTER TABLE `jobopportunities`
 ADD PRIMARY KEY (`jobOpportunitiesID`);

--
-- Indexes for table `message`
--
ALTER TABLE `message`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `researchassistances`
--
ALTER TABLE `researchassistances`
 ADD PRIMARY KEY (`researchassistancesID`);

--
-- Indexes for table `scholarshipfee`
--
ALTER TABLE `scholarshipfee`
 ADD PRIMARY KEY (`scholarshipFeeID`);

--
-- Indexes for table `subject`
--
ALTER TABLE `subject`
 ADD PRIMARY KEY (`subjectid`);

--
-- Indexes for table `tutionfee`
--
ALTER TABLE `tutionfee`
 ADD PRIMARY KEY (`tuitionFeeID`);

--
-- Indexes for table `tutionfeerange`
--
ALTER TABLE `tutionfeerange`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `university`
--
ALTER TABLE `university`
 ADD PRIMARY KEY (`universityid`);

--
-- Indexes for table `universitycampus`
--
ALTER TABLE `universitycampus`
 ADD PRIMARY KEY (`slno`);

--
-- Indexes for table `universitydegree`
--
ALTER TABLE `universitydegree`
 ADD PRIMARY KEY (`slno`);

--
-- Indexes for table `universitysubject`
--
ALTER TABLE `universitysubject`
 ADD PRIMARY KEY (`slno`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `accommodation`
--
ALTER TABLE `accommodation`
MODIFY `accommodationID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `application`
--
ALTER TABLE `application`
MODIFY `applicationid` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `apprequirements`
--
ALTER TABLE `apprequirements`
MODIFY `appRequirementsID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `city`
--
ALTER TABLE `city`
MODIFY `cityid` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=36;
--
-- AUTO_INCREMENT for table `country`
--
ALTER TABLE `country`
MODIFY `countryid` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `degree`
--
ALTER TABLE `degree`
MODIFY `degreeid` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `degreecontent`
--
ALTER TABLE `degreecontent`
MODIFY `slno` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `jobopportunities`
--
ALTER TABLE `jobopportunities`
MODIFY `jobOpportunitiesID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `message`
--
ALTER TABLE `message`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `researchassistances`
--
ALTER TABLE `researchassistances`
MODIFY `researchassistancesID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `scholarshipfee`
--
ALTER TABLE `scholarshipfee`
MODIFY `scholarshipFeeID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `subject`
--
ALTER TABLE `subject`
MODIFY `subjectid` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `tutionfee`
--
ALTER TABLE `tutionfee`
MODIFY `tuitionFeeID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `tutionfeerange`
--
ALTER TABLE `tutionfeerange`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `university`
--
ALTER TABLE `university`
MODIFY `universityid` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=46;
--
-- AUTO_INCREMENT for table `universitycampus`
--
ALTER TABLE `universitycampus`
MODIFY `slno` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `universitydegree`
--
ALTER TABLE `universitydegree`
MODIFY `slno` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `universitysubject`
--
ALTER TABLE `universitysubject`
MODIFY `slno` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
