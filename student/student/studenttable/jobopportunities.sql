-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Apr 29, 2015 at 07:54 PM
-- Server version: 5.6.20
-- PHP Version: 5.5.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `projectdb`
--

-- --------------------------------------------------------

--
-- Table structure for table `jobopportunities`
--

CREATE TABLE IF NOT EXISTS `jobopportunities` (
`jobOpportunitiesID` int(11) NOT NULL,
  `universityID` int(11) NOT NULL,
  `degreeID` int(11) NOT NULL,
  `jobOpportunities` text NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `jobopportunities`
--

INSERT INTO `jobopportunities` (`jobOpportunitiesID`, `universityID`, `degreeID`, `jobOpportunities`) VALUES
(1, 44, 10, '<ol><li><span style="line-height: 1.42857143;">Computer Engneer</span><br></li><li><span style="line-height: 1.42857143;">Mobile Engneer</span><br></li><li><span style="line-height: 1.42857143;">Web Developer</span><br></li></ol>');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `jobopportunities`
--
ALTER TABLE `jobopportunities`
 ADD PRIMARY KEY (`jobOpportunitiesID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `jobopportunities`
--
ALTER TABLE `jobopportunities`
MODIFY `jobOpportunitiesID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
