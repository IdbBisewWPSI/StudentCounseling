-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Apr 29, 2015 at 07:53 PM
-- Server version: 5.6.20
-- PHP Version: 5.5.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `projectdb`
--

-- --------------------------------------------------------

--
-- Table structure for table `degreecontent`
--

CREATE TABLE IF NOT EXISTS `degreecontent` (
`slno` int(11) NOT NULL,
  `universityID` int(11) NOT NULL,
  `degreeID` int(11) NOT NULL,
  `degreeContent` text NOT NULL
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `degreecontent`
--

INSERT INTO `degreecontent` (`slno`, `universityID`, `degreeID`, `degreeContent`) VALUES
(1, 44, 5, 'The University of Minnesota has been offering English as a Second Language courses and programs since 1968. Since then, we have helped thousands of students from across the globe achieve academic success, advance their careers, and develop international networks of friends.'),
(2, 44, 9, 'The University of Minnesota has been offering English as a Second Language courses and programs since 1968. Since then, we have helped .'),
(3, 45, 10, 'The mysqli_num_rows() function returns the number of rows in a The mysqli_num_rows() function returns the number of rows in a result set. result set. The mysqli_num_rows() function returns the number of rows in a result set. The University of Minnesota has been offering English as a Second Language courses and programs since 1968. Since then, we have helped thousands of students from across the globe achieve academic success, advance their careers, and develop international networks of friends.'),
(4, 45, 10, 'The University of Minnesota has been offering English as a Second Language courses and programs since 1968. Since then, we have helped thousands of students from across the globThe University of Minnesota has been offering English as a Second Language courses and programs since 1968. Since then, we have helped thousands of students from across the globe achieve academic success, advance their careers, and develop international networks of friends.');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `degreecontent`
--
ALTER TABLE `degreecontent`
 ADD PRIMARY KEY (`slno`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `degreecontent`
--
ALTER TABLE `degreecontent`
MODIFY `slno` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
