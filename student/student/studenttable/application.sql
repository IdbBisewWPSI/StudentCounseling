-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Apr 29, 2015 at 08:50 PM
-- Server version: 5.6.20
-- PHP Version: 5.5.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `projectdb`
--

-- --------------------------------------------------------

--
-- Table structure for table `application`
--

CREATE TABLE IF NOT EXISTS `application` (
`applicationid` int(11) NOT NULL,
  `fname` varchar(50) NOT NULL,
  `lname` varchar(50) NOT NULL,
  `countryid` int(11) NOT NULL,
  `locationid` int(11) NOT NULL,
  `state` varchar(50) NOT NULL,
  `postalcode` varchar(50) NOT NULL,
  `postaladdress` text NOT NULL,
  `phone` varchar(50) NOT NULL,
  `mobile` varchar(50) NOT NULL,
  `dob` varchar(30) NOT NULL,
  `gender` varchar(10) NOT NULL,
  `beginstudy` varchar(50) NOT NULL,
  `likedstudy` varchar(50) NOT NULL,
  `tuitionfee` varchar(20) NOT NULL,
  `test` varchar(20) NOT NULL,
  `score` varchar(30) NOT NULL,
  `appdate` varchar(30) NOT NULL,
  `comments` text NOT NULL,
  `email` varchar(200) NOT NULL,
  `status` varchar(20) NOT NULL
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `application`
--

INSERT INTO `application` (`applicationid`, `fname`, `lname`, `countryid`, `locationid`, `state`, `postalcode`, `postaladdress`, `phone`, `mobile`, `dob`, `gender`, `beginstudy`, `likedstudy`, `tuitionfee`, `test`, `score`, `appdate`, `comments`, `email`, `status`) VALUES
(1, 'Hafizur', 'Rahman', 0, 0, 'Dhaka', '1205', 'Dhanmondi', '6366546', '464646', '2015-04-15', '1', '2015-04-17', 'hafiz.ubikm@gmail.com', '10,000 - 15,000', 'ITEP', '5', '2015-04-15', 'sfsad sdfas fas asfas fasfa asfafa fasfas', 'hafiz.ubikm@gmail.com', 'Progress'),
(7, 'Delware', 'Sumon', 0, 0, 'Dhaka', '1205', 'Dhanmondi', '6366546', '01926743629', '2015-04-22', 'Male', '2015-04-20', 'hafiz.ubikm@gmail.com', '10,000 - 15,000', 'Pearson', '5', '2015-04-21', 'Other Comments or Questions in English', 'hafiz.ubikm@gmail.com', 'Active');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `application`
--
ALTER TABLE `application`
 ADD PRIMARY KEY (`applicationid`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `application`
--
ALTER TABLE `application`
MODIFY `applicationid` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
