-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Apr 29, 2015 at 08:51 PM
-- Server version: 5.6.20
-- PHP Version: 5.5.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `projectdb`
--

-- --------------------------------------------------------

--
-- Table structure for table `tutionfee`
--

CREATE TABLE IF NOT EXISTS `tutionfee` (
`tuitionFeeID` int(11) NOT NULL,
  `universityID` int(11) NOT NULL,
  `degreeID` int(11) NOT NULL,
  `tuitionFees` text NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `tutionfee`
--

INSERT INTO `tutionfee` (`tuitionFeeID`, `universityID`, `degreeID`, `tuitionFees`) VALUES
(1, 44, 3, '<ol><li><span style="line-height: 1.42857143;">Hostel 5000/=</span><br></li><li><span style="line-height: 1.42857143;">Dining 2000/=</span><br></li><li><span style="line-height: 1.42857143;">Londry 5000/=</span><br></li><li><span style="line-height: 1.42857143;">Mobile 6566/=</span><br></li><li><span style="line-height: 1.42857143;">Web design 2100/=</span><br></li></ol>'),
(2, 44, 2, '<ol><li><span style="line-height: 1.42857143;">Hostel 5000/=</span><br></li><li><span style="line-height: 1.42857143;">Dining 2000/=</span><br></li><li><span style="line-height: 1.42857143;">Londry 5000/=</span><br></li><li><span style="line-height: 1.42857143;">Mobile 6566/=</span><br></li><li><span style="line-height: 1.42857143;">Web design 2100/=</span><br></li></ol>'),
(3, 44, 2, '<ol><li><span style="line-height: 1.42857143;">Hostel 5000/=</span><br></li><li><span style="line-height: 1.42857143;">Dining 2000/=</span><br></li><li><span style="line-height: 1.42857143;">Londry 5000/=</span><br></li><li><span style="line-height: 1.42857143;">Mobile 6566/=</span><br></li><li><span style="line-height: 1.42857143;">Web design 2100/=</span><br></li></ol>'),
(4, 44, 3, '<ul><li><b style="color: rgb(255, 0, 0); line-height: 1.42857143;"><i style="color:rgb(255,0,0);"><u style="color:rgb(255,0,0);">Bangladesh</u></i></b><br></li><li><b style="color: rgb(255, 0, 0); line-height: 1.42857143;"><i style="color:rgb(255,0,0);"><u style="color:rgb(255,0,0);"><p style="color: rgb(255, 0, 0); display: inline !important;">India</p></u></i></b><br></li><li><b style="color: rgb(255, 0, 0); line-height: 1.42857143;"><i style="color:rgb(255,0,0);"><u style="color:rgb(255,0,0);"><p style="color: rgb(255, 0, 0); display: inline !important;">Pakistan</p></u></i></b><br></li><li><b style="color: rgb(255, 0, 0); line-height: 1.42857143;"><i style="color:rgb(255,0,0);"><u style="color:rgb(255,0,0);"><p style="color: rgb(255, 0, 0); display: inline !important;">butan</p></u></i></b><br></li></ul>'),
(5, 44, 2, '<h5><ul><li><span style="line-height: 1.1;">University of dhaka</span><br></li><li><span style="font-size: 14px; line-height: 1.42857143;">alskfjas</span><br></li><li><span style="font-size: 14px; line-height: 1.42857143;">asflkasjfl</span><br></li><li><span style="font-size: 14px; line-height: 1.42857143;">asdflasfjas</span><br></li><li><span style="font-size: 14px; line-height: 1.42857143;">as</span><br></li><li><span style="font-size: 14px; line-height: 1.42857143;">fasjflkasjf</span><br></li><li><span style="font-size: 14px; line-height: 1.42857143;">asfaslkfjasasfasff</span><br></li><li><span style="font-size: 14px; line-height: 1.42857143;">adflsafhlskdf</span><br></li></ul></h5>');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tutionfee`
--
ALTER TABLE `tutionfee`
 ADD PRIMARY KEY (`tuitionFeeID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tutionfee`
--
ALTER TABLE `tutionfee`
MODIFY `tuitionFeeID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
