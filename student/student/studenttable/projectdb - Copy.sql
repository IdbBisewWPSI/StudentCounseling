
-- Triggers `country`
--
DELIMITER //
CREATE TRIGGER `delcountry` AFTER DELETE ON `country`
 FOR EACH ROW BEGIN
DELETE FROM city WHERE city.countryid = old.countryid;
END
//
DELIMITER ;

-- --------------------------------------------------------





--
-- Stand-in structure for view `displaycity`
--

-- --------------------------------------------------------

--
-- Stand-in structure for view `displayinformation`
--

-- --------------------------------------------------------

--
-- Stand-in structure for view `disucampus`
--

-- --------------------------------------------------------

--
-- Stand-in structure for view `disudegree`
--

-- --------------------------------------------------------

--
-- Stand-in structure for view `disuniversity`
--

-- --------------------------------------------------------

--
-- Stand-in structure for view `disusubject`
--

-- --------------------------------------------------------

--
-- 

-

-- --------------------------------------------------------

--
-- Table structure for table `universitysubject`
--

-- --------------------------------------------------------

--
-- Structure for view `displaycity`
--

-- --------------------------------------------------------

--
-- Structure for view `displayinformation`
--


-- --------------------------------------------------------

--
-- Structure for view `disucampus`
--


-- --------------------------------------------------------

--
-- Structure for view `disudegree`
--


-- --------------------------------------------------------

--
-- Structure for view `disuniversity`
--


-- --------------------------------------------------------

--
-- Structure for view `disusubject`
--


--
-- Indexes for dumped tables
--

--
-- Indexes for table `accommodation`
--
ALTER TABLE `accommodation`
 ADD PRIMARY KEY (`accommodationID`);

--
-- Indexes for table `application`
--
ALTER TABLE `application`
 ADD PRIMARY KEY (`applicationid`);

--
-- Indexes for table `apprequirements`
--
ALTER TABLE `apprequirements`
 ADD PRIMARY KEY (`appRequirementsID`);

--
-- Indexes for table `city`
--
ALTER TABLE `city`
 ADD PRIMARY KEY (`cityid`);

--
-- Indexes for table `country`
--
ALTER TABLE `country`
 ADD PRIMARY KEY (`countryid`);

--
-- Indexes for table `degree`
--
ALTER TABLE `degree`
 ADD PRIMARY KEY (`degreeid`);

--
-- Indexes for table `degreecontent`
--
ALTER TABLE `degreecontent`
 ADD PRIMARY KEY (`slno`);

--
-- Indexes for table `jobopportunities`
--
ALTER TABLE `jobopportunities`
 ADD PRIMARY KEY (`jobOpportunitiesID`);

--
-- Indexes for table `message`
--
ALTER TABLE `message`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `researchassistances`
--
ALTER TABLE `researchassistances`
 ADD PRIMARY KEY (`researchassistancesID`);

--
-- Indexes for table `scholarshipfee`
--
ALTER TABLE `scholarshipfee`
 ADD PRIMARY KEY (`scholarshipFeeID`);

--
-- Indexes for table `subject`
--
ALTER TABLE `subject`
 ADD PRIMARY KEY (`subjectid`);

--
-- Indexes for table `tutionfee`
--
ALTER TABLE `tutionfee`
 ADD PRIMARY KEY (`tuitionFeeID`);

--
-- Indexes for table `tutionfeerange`
--
ALTER TABLE `tutionfeerange`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `university`
--
ALTER TABLE `university`
 ADD PRIMARY KEY (`universityid`);

--
-- Indexes for table `universitycampus`
--
ALTER TABLE `universitycampus`
 ADD PRIMARY KEY (`slno`);

--
-- Indexes for table `universitydegree`
--
ALTER TABLE `universitydegree`
 ADD PRIMARY KEY (`slno`);

--
-- Indexes for table `universitysubject`
--
ALTER TABLE `universitysubject`
 ADD PRIMARY KEY (`slno`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `accommodation`
--

--
-- AUTO_INCREMENT for table `application`
--

--
-- AUTO_INCREMENT for table `apprequirements`
--

--
-- AUTO_INCREMENT for table `city`
--

--
-- AUTO_INCREMENT for table `country`
--

--
-- AUTO_INCREMENT for table `degree`
--

--
-- AUTO_INCREMENT for table `degreecontent`
--

--
-- AUTO_INCREMENT for table `researchassistances`
--

--
-- AUTO_INCREMENT for table `tutionfee`
--

--
-- AUTO_INCREMENT for table `universitydegree`
--

--
-- AUTO_INCREMENT for table `universitysubject`
--

-- AUTO_INCREMENT for table `user`
--

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
