-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Apr 29, 2015 at 08:51 PM
-- Server version: 5.6.20
-- PHP Version: 5.5.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `projectdb`
--

-- --------------------------------------------------------

--
-- Table structure for table `universitycampus`
--

CREATE TABLE IF NOT EXISTS `universitycampus` (
`slno` int(11) NOT NULL,
  `universityid` int(11) NOT NULL,
  `imagetitle` varchar(200) NOT NULL,
  `campusimage` varchar(50) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `universitycampus`
--

INSERT INTO `universitycampus` (`slno`, `universityid`, `imagetitle`, `campusimage`) VALUES
(9, 44, 'Oxford Brookes University', 'campus01.jpg'),
(10, 44, 'City of Oxford College', 'campus03.jpg');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `universitycampus`
--
ALTER TABLE `universitycampus`
 ADD PRIMARY KEY (`slno`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `universitycampus`
--
ALTER TABLE `universitycampus`
MODIFY `slno` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
