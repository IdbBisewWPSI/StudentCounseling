<?php
include_once('pagesession.php');
include_once('../dbcon.php');
if(isset($_POST['submit'])){
	$logoname=$_FILES['logo']['name'];
	$logotemp=$_FILES['logo']['tmp_name'];
	$place_file= "images/$logoname";
	if (!file_exists($place_file)){
		move_uploaded_file($logotemp, $place_file); 
	}
	
	else {
		// first rename,
		$todays_date = date("mdYHis");
		$logoname=$todays_date.$logoname; //for send to database;
		$place_file ="images/$logoname";
		// and then move,
		move_uploaded_file($logotemp, $place_file);
	}
	
	$frontpage=$_FILES['frontpage']['name'];
	$tmpfrontpage=$_FILES['frontpage']['tmp_name'];
	$place_frontpage ="images/$frontpage";
	if (!file_exists($place_frontpage)){
		move_uploaded_file($tmpfrontpage, $place_frontpage); 
	}
	
	else {
		// first rename,
		$todays_date = date("mdYHis");
		$frontpage=$todays_date.$frontpage; //for send to database;
		$place_frontpage ="images/$frontpage";
		// and then move,
		move_uploaded_file($tmpfrontpage, $place_frontpage);
	}
	
	$query="insert  into university(logo,universityname,countryid,cityid,aboutuniversity,applicationdeadline,applicationperiod,email,web,fontpage,rank,rankimage) values('".$logoname."','".$_POST['txtuni']."','".$_POST['countryid']."','".$_POST['city']."','".$_POST['abtuni']."','".$_POST['txtdeadline']."','".$_POST['txtperiod']."','".$_POST['txtemail']."','".$_POST['txtweb']."','".$frontpage."','".$_POST['txtrank']."','".$_POST['rankimg']."')";
	$rstt=$mysqli->query($query);
}
?>
<!DOCTYPE html>
<html>
<head>
<title>Insert City</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="../css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="../css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
<link type="text/css" rel="stylesheet" href="../css/jquery-te-1.4.0.css">
<script language="javascript" type="text/javascript" src="../script/jquery.js"></script>
<script type="text/javascript" src="../script/jquery-te-1.4.0.min.js" charset="utf-8"></script>
<script language="javascript" type="text/javascript">
	var x=false;
	if(window.XMLHttpRequest){
		x=new XMLHttpRequest();
	}
	if(window.ActiveXObject){
		x=new ActiveXObject('Microsoft.XMLHTTP');
	}
	function getCity(citylist,targetlist){
		if(x){
			x.open('GET', citylist);
			x.onreadystatechange=function(){
				if(x.readyState==4 && x.status==200){
					document.getElementById('targetCity').innerHTML=x.responseText;
				}
			}
			x.send();
		}
	}
</script>


<style>
.rate{
    color:black;
    cursor:pointer;
}
.rate:hover{
    color:red;
}
.rate-item{
    cursor:pointer;
}
.rate-item:hover ~ .rate-item {
    color: black;
}
</style>
</head>
<body>

<h1 class="page-header">Insert New University</h1>
<div style="width:100% ">

<form name="form1" id="form1" method="post" action="" enctype="multipart/form-data">
  <table class="table-condensed table-hover ">
    <tr>
      <th>Logo</th>
      <td class="btn-file"><input name="logo" type="file" id="logo" ></td>
    </tr>
    <tr>
      <th>University Name</th>
      <td><input name="txtuni" type="text" id="txtuni" class="form-control" placeholder="Write The University Name" required></td>
    </tr>
    <tr>
      <th>Secect Country</th>
      <td>
	  <select name="countryid" id="countryid" class="form-control " onChange="getCity('cityforuni.php?country='+countryid.value,'targetCity')" required>
	  <option value="">--Select Country--</option>
	  	<?php
			$query="select * from country";
			$rst=$mysqli->query($query);
			while($row=$rst->fetch_row()){
		?>
		<option value="<?php echo $row[0];?>"><a href="#" class="form-control"><?php echo $row[1];?></a></option>
		<?php
		}
		?>
      </select></td>
    </tr>
    <tr>
      <th>Secect City </th>
      <td id="targetCity"><select name="city" class="form-control" id="city" required>
	  <option disabled value="">--Select Your City--</option>
		
      </select></td>
    </tr>
    <tr>
      <th>About University</th>
      <td><textarea rows="8" cols="70" name="abtuni" class="jqte-test"></textarea></td>
    </tr>
    <tr>
      <th>App Deadline</th>
      <td><input name="txtdeadline" type="date" id="txtdeadline" class="form-control"></td>
    </tr>
    <tr>
      <th>App Period</th>
      <td><input name="txtperiod" type="text" id="txtperiod" class="form-control"></td>
    </tr>
    <tr>
      <th>Email</th>
      <td><input name="txtemail" type="email" id="txtemail" class="form-control"></td>
    </tr>
    <tr>
      <th>Web</th>
      <td><input name="txtweb" type="text" id="txtweb" class="form-control"></td>
    </tr>
    <tr>
      <th>Front Page</th>
      <td><input name="frontpage" type="file" id="frontpage" required></td>
    </tr>
    <tr>
      <th>Rank</th>
      <td><input name="txtrank" type="text" id="txtrank" class="form-control" value="<?php echo rand(1,10)?>"></td>
    </tr>
    <tr>
      <th>Rank Image</th>
      <td><input type="hidden" value="<?php echo rand(1,10)?>" id="rankimg" name="rankimg"><!--<input name="rimage" type="file" id="rimage" class="form-control">-->
	  <span class="rate">
		  <span class="fa fa-star rate-item"></span> 
		  <span class="fa fa-star rate-item"></span> 
		  <span class="fa fa-star rate-item"></span> 
		  <span class="fa fa-star rate-item"></span> 
		  <span class="fa fa-star rate-item"></span> 
		  <span class="fa fa-star rate-item"></span> 
		  <span class="fa fa-star rate-item"></span> 
		  <span class="fa fa-star rate-item"></span> 
		  <span class="fa fa-star rate-item"></span> 
		  <span class="fa fa-star rate-item"></span>
	</span>
	  </td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td><input type="submit" name="submit" value="Submit University" class="btn btn-success btn-sm" />
			<a href="disuniversity.php" class="btn btn-info btn-sm">Display University</a></td>
    </tr>
    <tr>
      <th colspan="2"></th>
    </tr>
  </table>
</form>
</div>
<script>
	$('.jqte-test').jqte();
</script>
</body>
</html>