<?php
include_once('pagesession.php');
include_once('../dbcon.php');
if(isset($_POST['submit'])){
	$name=$_FILES['txtcampusimage']['name'];
	$temp=$_FILES['txtcampusimage']['tmp_name'];
	move_uploaded_file($temp,'images/'.$name);
	$query="insert  into universitycampus(universityid,imagetitle,campusimage) values(".$_POST['txtuniid'].",'".$_POST['txtcampustitle']."','".$name."')";
	$rstt=$mysqli->query($query);
}
?>
<!DOCTYPE html>
<html>
<head>
<title>Insert University Campus</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="../css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="../css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
</head>
<body>
<h1 class="page-header">Insert University Campus</h1>
<form name="form1" id="form1" method="post" action="" enctype="multipart/form-data">
  <table width="400" class="table-condensed table-hover ">
    <tr>
      <th>University Name </th>
      <td>
	  <select name="txtuniid" class="form-control " required>
	  <option value="">Select Your University</option>
	  	<?php
			$query="select * from university";
			$rst=$mysqli->query($query);
			while($row=$rst->fetch_row()){
		?>
		<option value="<?php echo $row[0];?>"><a href="#" class="form-control"><?php echo $row[2];?></a></option>
		<?php }	?>
      </select>
	  </td>
    </tr>
    <tr>
      <th>Campus Title </th>
      <td><input name="txtcampustitle" type="text" id="txtcampustitle" class="form-control" placeholder="Write The campus name" required></td>
    </tr>
    <tr>
      <th>Campus Image </th>
      <td><input name="txtcampusimage" type="file" id="txtcampusimage" class="form-control" required></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td><input type="submit" name="submit" value="Insert Campus" class="btn btn-success btn-sm"/>
			<a href="disucampus.php" class="btn btn-info btn-sm">Display University Campus</a></td>
    </tr>
    <tr>
      <th colspan="2"></th>
    </tr>
  </table>
</form>
</body>
</html>