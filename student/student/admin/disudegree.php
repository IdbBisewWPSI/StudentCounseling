<?php
include_once('pagesession.php');
include_once('../dbcon.php');
$query="select * from disudegree";
$rst=$mysqli->query($query);
?>
<!DOCTYPE html>
<html>
<head>
<title>Display University Degree</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="../css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="../css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
<style>
tr:nth-child(odd){background-color:#D9EDF7;}
tr:nth-child(even){background-color:#FCF8E3;}
th{background-color:#DFF0D8;}
</style>
</head>
<body>
<h1 class="page-header">Display University Degree</h1>
<form name="form1" method="post" action="">
	<a href="insudegree.php" class="btn btn-info btn-sm">Insert New University Degree</a>
	<a href="insudegreecbox.php" class="btn btn-danger btn-sm">Insert by CheckBox</a>
  <table class=" table table-bordered table-condensed table-hover ">
    <tr>
      <th>SLNo</th>
      <th>University Name</th>
      <th>Degree Name</th>
	  <th>Delete</th>
	  <th>Edit</th>
    </tr>
	<?php
		while($row=$rst->fetch_row()){
	?>
    <tr>
      <td><?php echo $row[0]?></td>
      <td><?php echo $row[1]?></td>
      <td><?php echo $row[2]?></td>
	  <td><a href="deludegree.php?id=<?php echo $row[0];?>" class="btn btn-danger  btn-xs"><span class="fa fa-times"></span></a></td>
	  <td><a href="edtudegree.php?id=<?php echo $row[0]?>" class="btn btn-info btn-xs"><span class="fa fa-pencil-square-o"></span></a></td>
    </tr>
	<?php
	}
	?>
  </table>
</form>
</body>
</html>
