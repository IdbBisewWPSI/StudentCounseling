<?php include_once('pagesession.php'); ?>
<!DOCTYPE html>
<html>
<head>
<title>Student Counselling</title>
<link rel="icon" href="../images/favicon.ico" type="image/x-icon"/>
<link href="../css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="../css/metisMenu.min.css" rel="stylesheet" type="text/css"/>
<link href="../css/sb-admin-2.css" rel="stylesheet" type="text/css"/>
<link href="../css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
<style>
.col-lg-12 iframe {position:absolute;top: 0;left: 0;width: 100%;height: 100%;}
.col-lg-12{position:relative;height:750px;overflow: hidden;}
iframe, object, embed {max-width: 100%;min-width: 100%;}
</style>
</head>
	<body>
	<div id="wrapper">
		<div class="navbar navbar-default navbar-static-top" role="navigation" style="background:#DFF0D8">
		<a class="navbar-brand" href="../index.php" target="_blank"><img style="float:left; margin-top:-12px; height:45px" src="images/logo.png"></a>
            <div class="navbar-header ">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Responsive Navigation</span>
                    <span class="icon-bar "></span>
                    <span class="icon-bar "></span>
                    <span class="icon-bar "></span>
                    <span class="icon-bar "></span>
                </button>
            </div>
			<ul class="nav navbar-top-links navbar-right ">
				<li>
					<a><b>Welcome</b> <i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-right"></i> <b><?php echo strtoupper($_SESSION['user']);?></b></a>
				</li>
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-envelope fa-fw"></i>  <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-messages">
					<?php 
						include_once('../dbcon.php');
						$message="select * from message order by id desc limit 3";
						$rstmsg=$mysqli->query($message);
						while($rowmsg=$rstmsg->fetch_row()){
					?>
                        <li>
                            <a href="#">
                                <div>
                                    <strong><?php echo $rowmsg[1]?></strong>
                                    <span class="pull-right text-muted">
                                        <em><?php echo $rowmsg[4]?></em>
                                    </span>
                                </div>
                                <div><?php echo substr("$rowmsg[3]",0,80)?></div>
                            </a>
                        </li>
                        <li class="divider"></li>
						<?php } ?>
                        <li>
                            <a class="text-center" href="#">
                                <strong>Read All Messages</strong>
                                <i class="fa fa-angle-right"></i>
                            </a>
                        </li>
                    </ul>
                    <!-- /.dropdown-messages -->
                </li>
				<li>
					<a><i class="fa fa-tasks fa-fw"></i>  <i class="fa fa-caret-down"></i></a>
				</li>
				<li>
					<a><i class="fa fa-bell fa-fw"></i>  <i class="fa fa-caret-down"></i></a>
				</li>
				<li>
					<a href="registration.php"><i class="fa fa-user-plus fa-fw"></i> <b>Create User</b></a>
				</li>				
				<li>
					<a href="logout.php"> <b>Log Out</b> <i class="fa fa-sign-out"></i></a>
				</li>
			</ul>
			
            <div class="navbar-default sidebar " role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">				
                        <li>
                            <a href="indexi.php"><i class="fa fa-dashboard fa-fw"></i> <b>Dashboard</b></a>
                        </li>
                        <li>
							<a href="disapplications.php" target="divId" ><i class="fa fa-file-text fa-fw"></i> <b>Applications</b><span class="fa arrow"></span></a>
                        </li>
						<li>
							<a href="discountry.php" target="divId" > <b>Countrys</b><span class="fa arrow"></span></a>
						</li>
						<li>
							<a href="discity.php" target="divId"> <b>Cities</b><span class="fa arrow"></span></a>
						</li>
						<li>
							<a href="disuniversity.php" target="divId" > <b>University Detail</b><span class="fa arrow"></span></a>
						</li>
						<li>
							<a href="disucampus.php" target="divId" > <b>University Campus</b><span class="fa arrow"></span></a>
						</li>
						<li>
							<a href="disusubject.php" target="divId" > <b>University Subject</b><span class="fa arrow"></span></a>
						</li>
						<li>
							<a href="disudegree.php" target="divId" > <b>University Degree</b><span class="fa arrow"></span></a>
						</li>
						<li>
							<a href="distutionfee.php" target="divId" > <b>Tution Fee</b><span class="fa arrow"></span></a>
						</li>
						<li>
							<a href="disapprequirements.php" target="divId" > <b>App Requirement</b><span class="fa arrow"></span></a>
						</li>
						<li>
							<a href="disaccommodation.php" target="divId" > <b>Accommodation</b><span class="fa arrow"></span></a>
						</li>
						<li>
							<a href="disjobopportunities.php" target="divId" > <b>Job Opportunities</b><span class="fa arrow"></span></a>
						</li>
						<li>
							<a href="disresearchassistances.php" target="divId" > <b>Research Assistances</b><span class="fa arrow"></span></a>
						</li>
						<li>
							<a href="dissubject.php" target="divId" > <b>Subjects</b><span class="fa arrow"></span></a>
						</li>
						<li>
							<a href="disdegree.php" target="divId" > <b>Degree</b><span class="fa arrow"></span></a>
						</li>
                    </ul>
                </div>
            </div>
		</div>

        <div id="page-wrapper" class="">
            <div class="container-fluid ">
                <div class="row">
                    <div class="col-lg-12">
						<iframe id="divId" name="divId" srcdoc="<center><h1>Welcome to Admin Panel</h1></center>" frameborder="0">
							
						</iframe>
                    </div>
                </div>
            </div>
        </div>
	</div>
<script src="../script/jquery.js"></script>
<script src="../script/bootstrap.min.js"></script>
<script src="../script/metisMenu.min.js"></script>
<script src="../script/sb-admin-2.js"></script>
</body>
</html>