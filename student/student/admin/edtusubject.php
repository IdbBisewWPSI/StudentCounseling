<?php
include_once('pagesession.php');
include_once('../dbcon.php');
if(isset($_POST['submit'])){
	$id=$_GET['id'];
	$q="select * from universitysubject where slno=".$_GET['id']."";
	$r=$mysqli->query($q);
	$row=$r->fetch_row();
	$query="insert  into universitysubject(univercityid,subjectid) values(".$_POST['txtuniid'].",".$_POST['txtsubid'].")";
	$rsttt=$mysqli->query($query);
}
?>
<!DOCTYPE html>
<html>
<head>
<title>Insert University Subject</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="../css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="../css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
</head>
<body>
<h1 class="page-header">Insert University Subject</h1>
<form name="form1" id="form1" method="post" action="" enctype="multipart/form-data">
  <table width="400" class="table-condensed table-hover ">
    <tr>
      <th>University Name</th>
      <td><select name="txtuniid" class="form-control " required>
	  <option value="">Select University</option>
	  	<?php
			$queryy="select * from university";
			$rst=$mysqli->query($queryy);
			while($row=$rst->fetch_row()){
		?>
		<option value="<?php echo $row[0];?>"><a href="#" class="form-control"><?php echo $row[2];?></a></option>
		<?php
		}
		?>
      </select></td>
    </tr>
    <tr>
      <th>Subject Name</th>
      <td><select name="txtsubid" class="form-control " required>
	  <option value="">Select Subject</option>
	  	<?php
			$queryyy="select * from subject";
			$rstt=$mysqli->query($queryyy);
			while($roww=$rstt->fetch_row()){
		?>
		<option value="<?php echo $roww[0];?>"><a href="#" class="form-control"><?php echo $roww[1];?></a></option>
		<?php
		}
		?>
      </select></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td><input type="submit" name="submit" value="Insert Subject" class="btn btn-success btn-sm"/>
			<a href="disusubject.php" class="btn btn-info btn-sm">Display University Subject</a></td>
    </tr>
    <tr>
      <th colspan="2"></th>
    </tr>
  </table>
</form>
</body>
</html>