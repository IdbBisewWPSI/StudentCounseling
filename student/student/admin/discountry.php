<?php
include_once('pagesession.php');
include_once('../dbcon.php');
$query="select * from country";
$rst=$mysqli->query($query);
?>
<!DOCTYPE html>
<html>
<head>
<title>Display Country</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="../css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="../css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
</head>
<style>
.fa-trash-o{color:red;font-size:20px}
.fa-pencil-square-o{color:#D2762D;font-size:20px}
tr:nth-child(odd){background-color:#D9EDF7;}
tr:nth-child(even){background-color:#FCF8E3;}
th{background-color:#DFF0D8;}
</style>
<body>
<h1 class="">Display Country</h1>
<form name="form1" method="post" action="">
	<a href="inscountry.php" class="btn btn-info btn-sm">Insert New Country</a>
  <table class=" table table-bordered table-condensed table-hover ">
    <tr>
      <th>CountryID</th>
      <th>Country Name</th>
	  <th>Delete</th>
	  <th>Edit</th>
    </tr>
	<?php
		while($row=$rst->fetch_row()){
	?>
    <tr>
      <td><?php echo $row[0]?></td>
      <td><?php echo $row[1]?></td>
	  <td><a href="delcountry.php?id=<?php echo $row[0];?>" class="btn btn-danger  btn-xs"><span class="fa fa-trash"></span></a></td>
	  <td><a href="edtcountry.php?id=<?php echo $row[0]?>" class="btn btn-info btn-xs"><span class="fa fa-pencil-square"></span></a></td>
    </tr>
	<?php
	}
	?>
  </table>
</form>
</body>
</html>
