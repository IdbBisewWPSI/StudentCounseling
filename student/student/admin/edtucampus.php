<?php
include_once('pagesession.php');
include_once('../dbcon.php');
$id=$_GET['id'];
$q="select * from universitycampus where slno=".$id."";
$r=$mysqli->query($q);
$row=$r->fetch_row();

if(isset($_POST['Submit'])){
	$name=$_FILES['txtcampusimage']['name'];
	$temp=$_FILES['txtcampusimage']['tmp_name'];
	move_uploaded_file($temp,'images/'.$name);
	$q="update  universitycampus set universityid=".$_POST['txtuniid'].", imagetitle='".$_POST['txtcampustitle']."', campusimage='".$name."' where slno=".$id."";
	$rst=$mysqli->query($q);
	header("location:disucampus.php");
}
?>
<!DOCTYPE html>
<html>
<head>
<title>Update University Campus</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="../css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="../css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
</head>
<body>
<h1 class="page-header">Update University Campus</h1>
<form name="form1" method="post" action="" enctype="multipart/form-data">
<input type="hidden" name="hd" id="hd" value="<?php echo $row[0]?>">
  <table width="400" class="table-condensed table-hover ">
      <tr>
      <th>University Name </th>
      <td><select name="txtuniid" class="form-control ">
	  	<?php
			$query="select * from university";
			$rst=$mysqli->query($query);
			while($row=$rst->fetch_row()){
		?>
		<option value="<?php echo $row[0];?>"><?php echo $row[2];?></option>
		<?php
		}
		?>
      </select></td>
    </tr>
    <tr>
      <th>Campus Title </th>
      <td><input name="txtcampustitle" type="text" id="txtcampustitle" class="form-control" placeholder="Write The Campus name" value="<?php echo $row[2]?>"></td>
    </tr>
    <tr>
      <th>Campus Image </th>
      <td><input name="txtcampusimage" type="file" id="txtcampusimage" class="form-control"></td>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td><input type="submit" name="Submit" value="Submit" class="btn btn-success btn-sm"></td>
    </tr>
  </table>
</form>
</body>
</html>
