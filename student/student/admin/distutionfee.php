<?php
include_once('pagesession.php');
include_once('../dbcon.php');
$query="select * from tutionfee";
$rst=$mysqli->query($query);
?>
<!DOCTYPE html>
<html>
<head>
<title>Display Tuition Fee</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="../css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="../css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
<style>
tr:nth-child(even){background-color:#FCF8E3;}
tr:nth-child(odd){background-color:#D9EDF7;}
th{background-color:#DFF0D8;}
</style>
</head>
<body>
<h1 class="page-header">Display Tution Fee</h1>
<form name="form1" method="post" action="">
	<a href="instutionfee.php" class="btn btn-info btn-sm">Insert Tuition Fee</a>
  <table class=" table table-bordered table-condensed table-hover ">
    <tr>
      <th>University Name</th>
      <th>Degree Name</th>
      <th>Living Cost</th>
      <th>Tution Fee</th>
	  <th>Delete</th>
	  <th>Edit</th>
    </tr>
	<?php
		while($row=$rst->fetch_row()){
	?>
    <tr>
      <td><?php echo $row[1]?></td>
      <td><?php echo $row[2]?></td>
      <td><?php echo $row[4]?></td>
      <td><?php echo $row[3]?></td>
	  <td><a href="deltuitionfee.php?id=<?php echo $row[0];?>" class="btn btn-danger  btn-xs"><span class="fa fa-times"></span></a></td>
	  <td><a href="edtdegree.php?id=<?php echo $row[0]?>" class="btn btn-info btn-xs"><span class="fa fa-pencil-square-o"></span></a></td>
    </tr>
	<?php
	}
	?>
  </table>
</form>
</body>
</html>
