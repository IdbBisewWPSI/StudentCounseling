<?php
include_once('pagesession.php');
include_once('../dbcon.php');
$query="select * from disuniversity";
$rst=$mysqli->query($query);
?>
<!DOCTYPE html>
<html>
<head>
<title>Display University</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="../css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="../css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
<script src="../script/jquery.js" type="text/javascript"></script>
<script src="../script/main.js" type="text/javascript"></script>
<style>
tr:nth-child(odd){background-color:#D9EDF7;}
tr:nth-child(even){background-color:#FCF8E3;}
th{background-color:#DFF0D8;}

/*Desigh for hovering images*/
#screenshot{
	position:absolute;
	border:1px solid #ccc;
	background:#333;
	padding:5px;
	display:none;
	color:#fff;
	}
</style>
</head>
<body>
<h1 class="page-header">Display University</h1>
<form name="form1" method="post" action="" >
	<a href="insuniversity.php" class="btn btn-info btn-sm">Insert New University</a>
	<div class="table-responsive">
  <table id="myTable" class=" table table-bordered table-condensed table-hover tablesorter">
  <thead> 
    <tr>
      <th>Logo</th>
      <th>University</th>
      <th>Country</th>
      <th>City</th>
      <th>About University</th>
      <th>Dead Line</th>
      <th>Email</th>
      <th>Web</th>
      <th>Font Page</th>
      <th>Rank</th>
      <th>Rank Image</th>
	  <th>Delete</th>
	  <th>Edit</th>
    </tr>
</thead>
<tbody>
	<?php
		while($row=$rst->fetch_row()){
	?>
    <tr>
      <td><a href="#" title="<?php echo $row[2]?>" class="screenshot" rel="images/<?php echo $row[1]?>"><img src="images/<?php echo $row[1]?>" width="40px" height="40px"></a></td>
	  <td><?php echo $row[2]?></td>
	  <td><?php echo $row[3]?></td>
	  <td><?php echo $row[4]?></td>
	  <td><?php echo substr($row[5],0,30)?></td>
	  <td><?php echo $row[6]?></td>
	  <td><?php echo $row[7]?></td>
	  <td><?php echo $row[8]?></td>
	  <td><a href="#" title="<?php echo $row[2]?>" class="screenshot" rel="images/<?php echo $row[9]?>"><img src="images/<?php echo $row[9]?>" height="40px"></a></td>
	  <td><?php echo $row[10]?></td>
	  <td><?php echo $row[11]?></td>
	  <td><a href="deluniversity.php?id=<?php echo $row[0].','.$row[1].','.$row[9];?>" class="btn btn-danger  btn-xs"><span class="fa fa-times"></span></a></td>
	  <td><a href="edtuniversity.php?id=<?php echo $row[0].','. $row[3];?>" class="btn btn-info btn-xs"><span class="fa fa-pencil-square-o"></span></a></td>
    </tr>
	<?php
	}
	?>
<tbody>
  </table>
  </div>
</form>
</body>
</html>


























