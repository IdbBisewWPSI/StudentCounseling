<?php
include_once('pagesession.php');
include_once('../dbcon.php');
$id=$_GET['id'];
$xquery="select * from university where universityid='".$id."'";
$result=$mysqli->query($xquery);
$rows=$result->fetch_row();
if(isset($_POST['submit'])){
	if($_POST['logo']==$rows[1]){
	$query="update university set logo='".$_POST['logo']."', universityname='".$_POST['txtuni']."', countryid='".$_POST['countryid']."',cityid='".$_POST['city']."',aboutuniversity='".$_POST['abtuni']."',applicationdeadline='".$_POST['txtdeadline']."', applicationperiod='".$_POST['txtperiod']."', email='".$_POST['txtemail']."', web='".$_POST['txtweb']."', fontpage='".$_POST['txtfontpage']."', rank='".$_POST['txtrank']."', rankimage='".$_POST['rankimg']."' where universityid='".$id."'";
	$rstt=$mysqli->query($query);
	}
else{
	$logoname=$_FILES['logo']['name'];
	$logotemp=$_FILES['logo']['tmp_name'];
	move_uploaded_file($logotemp,'images/'.$logoname);
	$query="update university set logo='".$logoname."', universityname='".$_POST['txtuni']."', countryid='".$_POST['countryid']."',cityid='".$_POST['city']."',aboutuniversity='".$_POST['abtuni']."',applicationdeadline='".$_POST['txtdeadline']."', applicationperiod='".$_POST['txtperiod']."', email='".$_POST['txtemail']."', web='".$_POST['txtweb']."', fontpage='".$_POST['txtfontpage']."', rank='".$_POST['txtrank']."', rankimage='".$_POST['rankimg']."' where universityid='".$id."'";
	$rstt=$mysqli->query($query);
	}
}
?>

<!DOCTYPE html>
<html>
<head>
<title>New Application</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="../css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="../css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
<script language="javascript" type="text/javascript" src="../script/jquery.js"></script>
<!--<script language="javascript" type="text/javascript" src="../../jQueryy/jquery-1.6.4.min.js"></script>-->
<script src="../script/main.js" type="text/javascript"></script>
<script language="javascript" type="text/javascript">
	var x=false;
	if(window.XMLHttpRequest){
		x=new XMLHttpRequest();
	}
	if(window.ActiveXObject){
		x=new ActiveXObject('Microsoft.XMLHTTP');
	}
	function getCity(citylist,targetlist){
		if(x){
			x.open('GET', citylist);
			x.onreadystatechange=function(){
				if(x.readyState==4 && x.status==200){
					document.getElementById('targetCity').innerHTML=x.responseText;
				}
			}
			x.send();
		}
	}
	
	function valid(){
	var x=document.form1.txtcity.value;
	if(x==""){
		alert('Must not be undifiend');
		document.form1.txtuni.focus();
		return false;
	}
	}
</script>
</head>
<body>
<h1 class="">New Application</h1>
	<div class="col-md-3 pad col-xs-12 col-sm-12">
		<div class="apply">
			<div class="apply_inner">
				<div class="apply_form">
					<form action="#" method="post">
						<p style="color:red"><b>Please complete this form in English. (*=Required field.)</b> </p>
						 <div class="form-group">
							<label for="firstname">First name<span class="star">*</span></label>
							<input type="text" class="form-control" name="fname" id="name" placeholder="First Name" required>
						  </div>
						 <div class="form-group">
							<label for="lastname">Last name<span class="star">*</span></label>
							<input type="text" class="form-control" name="lname" id="laname" placeholder="Last Name" required>
						  </div>
						 <div class="form-group">
							<label for="country">Country<span class="star">*</span></label>
							  <select name="country" class="form-control " id="country" required>
							   <option value=""  >Select Your Country</option>
								<?php
									$countryquery="select * from country";
									$countryrst=$mysqli->query($countryquery);
									while($courtryrow=$countryrst->fetch_row()){
								?>
								<option value="<?php echo $courtryrow[1];?>"><?php echo $courtryrow[1];?></option>
								<?php }	?>
							  </select>
						  </div>
						 <div class="form-group">
							<label for="location">Location</label>
							<select name="location" id="location" class="form-control">
								<option value="">Select Your Location</option>
							</select>
						  </div>
						 <div class="form-group">
							<label for="state">State/province</label>
							<input type="text" class="form-control" name="state" id="state" placeholder="State">
						  </div>
						 <div class="form-group">
							<label for="postal">Postal code</label>
							<input type="text" class="form-control" id="postalcode" name="postalcode" placeholder="Postal Code">
						  </div>
						 <div class="form-group">
							<label for="postaladd">Postal address</label>
							<input type="text" class="form-control" name="postaladdress" id="postal" placeholder="Postal Address">
						  </div>
						 <div class="form-group">
							<label for="telephone">Telephone</label>
							<input type="text" class="form-control" name="phone" id="phone" placeholder="Telephone">
						  </div>
						 <div class="form-group">
							<label for="mobile">Mobile Number</label>
							<input type="text" class="form-control" name="mobile" id="mobile" placeholder="Mobile Number">
						  </div>
						 <div class="form-group">
							<label for="birth">Date of birth</label>
							<input type="date" class="form-control" name="dob" id="mobile" placeholder="Date of birth">
						  </div>
						 <div class="form-group">
							<label for="gender">Gender<span class="star">*</span></label>
							<input type="radio" id="gender" name="gender" value="Male"  required> Male 
							<input type="radio" id="gender" name="gender" value="Female"> Female <br>
						  </div>
						 <div class="form-group">
							<label for="time">When do you plan to begin your program of study? </label>
							<input type="date" class="form-control" name="beginstudy" id="time" placeholder="">
						  </div>
						 <div class="form-group">
							<label for="like">What would you like to study?<span class="star">*</span>  </label>
							<input type="text" class="form-control" id="like" name="likedstudy" placeholder="" required>
						  </div>
						 <div class="form-group">
							<label for="pay">How much can you afford to pay per year for tuition? <span class="star" required>*</span></label>
							<select id="pay" name="tuitionfee"  class="form-control">
								<option value="10,000 - 15,000">10,000 - 15,000</option>
								<option value="10,000 - 15,000">15,000 - 20,000</option>
								<option value="10,000 - 15,000">20,000 - 25,000</option>
								<option value="10,000 - 15,000">25,000 - 30,000</option>
								<option value="10,000 - 15,000">30,000 - 35,000</option>
							</select>
						  </div>
						 <div class="form-group">
							<label for="test">If Any Test ?</label>
							<select name="test" id="pay"  class="form-control">
								<option value="ACT">ACT</option>
								<option value="GMAT">GMAT</option>
								<option value="GRE">GRE</option>
								<option value="IELTS">IELTS</option>
								<option value="ITEP">ITEP</option>
								<option value="Pearson">Pearson</option>
								<option value="SAT">SAT</option>
								<option value="TOFEL">TOFEL</option>
							</select>
						  </div>
						 <div class="form-group">
							<label for="score">Score</label>
							<input type="text" class="form-control" name="score" id="score" placeholder="">
						  </div>
						  
						 <div class="form-group">
							<label for="datetaken">Date Taken</label>
							<input type="date" class="form-control" id="datetaken" name="appdate" placeholder="">
						  </div>

						 <div class="form-group">
							<label for="comments">Other Comments or Questions (in English) </label>
							<textarea class="form-control" rows="3" name="comments"></textarea>
						  </div>
						<div class="form-group">
							<label for="email1">Email address<span class="star">*</span></label>
							<input type="email" class="form-control" id="Email1" name="email" placeholder="Enter email" required>
						 </div>
						<div class="form-group">
							<label for="emailcon">Conform email address<span class="star">*</span></label>
							<input type="email" class="form-control" id="Email2" name="likedstudy" placeholder="Conform email" required>
						 </div>
						 <input class="btn btn-default" type="submit" name="submitapp" value="Submit"/>
					</form>
				</div>
			</div>
		</div>
	</div>
</body>
</html>