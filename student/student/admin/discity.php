<?php
include_once('pagesession.php');
include_once('../dbcon.php');
$query="select * from displaycity";
$rst=$mysqli->query($query);
?>
<!DOCTYPE html>
<html>
<head>
<title>Display City</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="otherpage/multi_delete/Multi_Delete/css/bootstrap.css" type="text/css" rel="stylesheet"/>
<link href="otherpage/multi_delete/Multi_Delete/css/DT_bootstrap.css" type="text/css" rel="stylesheet"/>
<link href="../css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
<script src="otherpage/multi_delete/Multi_Delete/js/jquery.js" type="text/javascript" language="javascript"></script>
<script src="otherpage/multi_delete/Multi_Delete/js/bootstrap.js" type="text/javascript" language="javascript"></script>
<script src="otherpage/multi_delete/Multi_Delete/js/jquery.dataTables.js" type="text/javascript" language="javascript"></script>
<script src="otherpage/multi_delete/Multi_Delete/js/DT_bootstrap.js" type="text/javascript" language="javascript"></script>

<style>
tr:nth-child(odd){background-color:#D9EDF7;}
tr:nth-child(even){background-color:#FCF8E3;}
th{background-color:#DFF0D8;}
</style>
</head>
<body>
    <div class="row-fluid">
        <div class="span12">
            <div class="container">
				<h1 class="">Display City</h1>
					<form method="post" action="delall.php" >
						<a href="inscity.php" class="btn btn-info btn-sm">Insert New City</a>
                        <table cellpadding="0" cellspacing="0" border="0" class="table  table-striped table-bordered table-hover" id="example">
                            <div class="alert alert-info">
                                <strong><i class="icon-user icon-large"></i>&nbsp;To Delete Multiple Data :</strong>
								&nbsp;&nbsp;Check the radion Button and click the Delete button to Delete Data 
                            </div>
                            <thead>
								<tr>
								  <th>CityID</th>
								  <th><input type="checkbox"></th>
								  <th>Country Name</th>
								  <th>City Name </th>
								  <th>Delete</th>
								  <th>Edit</th>
								</tr>
                            </thead>
                            <tbody>
								<?php
									while($row=$rst->fetch_row()){
								?>
								<tr>
								  <td><?php echo $row[0]?></td>
								  <td><input type="checkbox" name="delv[]" id="delv" value="<?php echo $row[0]?>"></td>
								  <td><?php echo $row[1]?></td>
								  <td><?php echo $row[2]?></td>
								  <td><a href="delcity.php?id=<?php echo $row[0];?>" class="btn btn-danger  btn-xs"><span class="fa fa-times"></span></a></td>
								  <td><a href="edtcity.php?id=<?php echo $row[0]?>" class="btn btn-info btn-xs"><span class="fa fa-pencil-square-o"></span></a></td>
								</tr>
								<?php
								}
								?>
                            </tbody>
                        </table>
						<input type="submit" class="btn btn-danger" value="Delete" name="delete">
					</form>
        </div>
        </div>
        </div>
    </div>
</body>
</html>
