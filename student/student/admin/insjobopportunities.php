<?php
include_once('pagesession.php');
include_once('../dbcon.php');
if(isset($_POST['submit'])){
	$query="insert  into jobopportunities (universityID,degreeID,jobOpportunities) values('".$_POST['univercityid']."','".$_POST['degree']."','".$_POST['jobopportunities']."')";
	$rstt=$mysqli->query($query);
}
?>
<!DOCTYPE html>
<html>
<head>
<title>Insert Jobopportunities</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="../css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="../css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
<link type="text/css" rel="stylesheet" href="../css/jquery-te-1.4.0.css">
<script language="javascript" type="text/javascript" src="../script/jquery.js"></script>
<script type="text/javascript" src="../script/jquery-te-1.4.0.min.js" charset="utf-8"></script>
<script language="javascript" type="text/javascript">
	var x=false;
	if(window.XMLHttpRequest){
		x=new XMLHttpRequest();
	}
	if(window.ActiveXObject){
		x=new ActiveXObject('Microsoft.XMLHTTP');
	}
	function getCity(citylist,targetlist){
		if(x){
			x.open('GET', citylist);
			x.onreadystatechange=function(){
				if(x.readyState==4 && x.status==200){
					document.getElementById('targetDegree').innerHTML=x.responseText;
				}
			}
			x.send();
		}
	}
</script>
</head>
<body>

<h1 class="page-header">Insert App Requirements</h1>
<div style="width:100% ">

<form name="form1" id="form1" method="post" action="" enctype="multipart/form-data">
  <table class="table-condensed table-hover ">
	<tr>
		<th>Select University</th>
	</tr>
    <tr>
      
      <td>
	  <select name="univercityid" id="univercityid" class="form-control " onChange="getCity('degreeforuni.php?univercityid='+univercityid.value,'targetDegree')" required>
	  <option value="">--Select University--</option>
	  	<?php
			$query="select * from university";
			$rst=$mysqli->query($query);
			while($row=$rst->fetch_row()){
		?>
		<option value="<?php echo $row[0];?>"><a href="#" class="form-control"><?php echo $row[2];?></a></option>
		<?php
		}
		?>
      </select></td>
    </tr>
	<tr>
      <th>Select Degree</th>
	</tr>
    <tr>
      <td id="targetDegree"><select name="degree" class="form-control" id="degree" required>
	  <option disabled value="">--Select Degree--</option>
		
      </select></td>
    </tr>
	<tr>
      <th>About Jobopportunities</th>
	</tr>
    <tr>

      <td><textarea rows="8" cols="70" name="jobopportunities" class="jqte-test" value=""></textarea></td>
    </tr>
	<tr>      <td>&nbsp;</td></tr>
    <tr>

      <td><input type="submit" name="submit" value="Submit App Requirements" class="btn btn-success btn-sm" />
			<a href="disjobopportunities.php" class="btn btn-info btn-sm">Display Jobopportunities</a>
		</td>
    </tr>
    <tr>
      <th colspan="2"></th>
    </tr>
  </table>
</form>
</div>
	<script>
	$('.jqte-test').jqte();
</script>
</body>
</html>