<!doctype html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="apple-touch-icon" href="apple-touch-icon.png">
        <!-- Place favicon.ico in the root directory -->

        <link rel="stylesheet" href="css/normalize.css">
        <link rel="stylesheet" href="css/main.css">
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/font-awesome.min.css">
		<link href="css/jquery.bxslider.css" rel="stylesheet" />
        <link rel="stylesheet" href="style.css">
        <link rel="stylesheet" href="css/responsive.css">
        <script src="js/vendor/modernizr-2.8.3.min.js"></script>
    </head>
    <body>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

       <div class="main-container">
			<div class="container">
				<header class="header_area">
					<div class="header_inner">
						<div class="row">
							<div class="col-md-3">
								<div class="social">
									<ul>
										<li><a href="#"><i class="fa fa-facebook"></i></a></li>
										<li><a href="#"><i class="fa fa-twitter"></i></a></li>
										<li><a href="#"><i class="fa fa-pinterest-p"></i></a></li>
										<li><a href="#"><i class="fa fa-google-plus"></i></a></li>
										<li><a href="#"><i class="fa fa-dribbble"></i></a></li>
									</ul>
								</div>
							</div>
							<div class="col-md-9 mail">
								<div class="mail_area">
									<ul>
										<li><a href="mailto:#"><i class="fa fa-envelope-o"></i> abc@gmail.com</a></li>
										<li><a href="#"><i class="fa fa-phone"></i> +880 191 2578522</a></li>
									</ul>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-3 pad">
								<div class="logo">
									<img src="images/logo.png" alt="" />
								</div>
							</div>
							<div class="col-md-9 pad">
								<nav class="navbar navbar-default">
								  <div class="container-fluid">
									<!-- Brand and toggle get grouped for better mobile display -->
									<div class="navbar-header">
									  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
										<span class="sr-only">Toggle navigation</span>
										<span class="icon-bar"></span>
										<span class="icon-bar"></span>
										<span class="icon-bar"></span>
									  </button>
									</div>

									<!-- Collect the nav links, forms, and other content for toggling -->
									<div class="collapse navbar-collapse menu" id="bs-example-navbar-collapse-1">
									  <ul class="nav navbar-nav">
										<li><a href="#">Home</a></li>
										<li><a href="#">About</a></li>
										<li><a href="#">Our Campuses</a></li>
										<li><a href="#">Tution fees</a></li>
									  </ul>
									</div><!-- /.navbar-collapse -->
								  </div><!-- /.container-fluid -->
								</nav>
							</div>
						</div>
					</div>
				</header> <!-- header area ends here -->
				<section class="banner_area">
					<div class="left_form_area">
						<div class="row">
							<div class="col-md-3">
								<div class="form">
									<form action="#">
										<div class="select_country">
											<label for="country">Country</label>
										<select name="country" id="country" class="form-control">
											<option value="AFG">Afghanistan</option>
											<option value="ALA">Åland Islands</option>
											<option value="ALB">Albania</option>
											<option value="DZA">Algeria</option>
											<option value="ASM">American Samoa</option>
											<option value="AND">Andorra</option>
											<option value="AGO">Angola</option>
											<option value="AIA">Anguilla</option>
											<option value="ATA">Antarctica</option>
											<option value="ATG">Antigua and Barbuda</option>
											<option value="ARG">Argentina</option>
											<option value="ARM">Armenia</option>
											<option value="ABW">Aruba</option>
											<option value="AUS">Australia</option>
											<option value="AUT">Austria</option>
											<option value="AZE">Azerbaijan</option>
											<option value="BHS">Bahamas</option>
											<option value="BHR">Bahrain</option>
											<option value="BGD">Bangladesh</option>
											<option value="BRB">Barbados</option>
											<option value="BLR">Belarus</option>
											<option value="BEL">Belgium</option>
											<option value="BLZ">Belize</option>
											<option value="BEN">Benin</option>
											<option value="BMU">Bermuda</option>
											<option value="BTN">Bhutan</option>
											<option value="BOL">Bolivia</option>
											<option value="BES">Bonaire</option>
											<option value="BIH">Bosnia and Herzegovina</option>
											<option value="BWA">Botswana</option>
											<option value="BVT">Bouvet Island</option>
											<option value="BRA">Brazil</option>
											<option value="IOT">British Indian Ocean Territory</option>
											<option value="BRN">Brunei Darussalam</option>
											<option value="BGR">Bulgaria</option>
											<option value="BFA">Burkina Faso</option>
											<option value="BDI">Burundi</option>
											<option value="KHM">Cambodia</option>
											<option value="CMR">Cameroon</option>
											<option value="CAN">Canada</option>
											<option value="CPV">Cape Verde</option>
											<option value="CYM">Cayman Islands</option>
											<option value="CAF">Central African Republic</option>
											<option value="TCD">Chad</option>
											<option value="CHL">Chile</option>
											<option value="CHN">China</option>
											<option value="CXR">Christmas Island</option>
											<option value="CCK">Cocos (Keeling) Islands</option>
											<option value="COL">Colombia</option>
											<option value="COM">Comoros</option>
											<option value="COG">Congo</option>
											<option value="COD">Congo</option>
											<option value="COK">Cook Islands</option>
											<option value="CRI">Costa Rica</option>
											<option value="CIV">Côte d'Ivoire</option>
											<option value="HRV">Croatia</option>
											<option value="CUB">Cuba</option>
											<option value="CUW">Curaçao</option>
											<option value="CYP">Cyprus</option>
											<option value="CZE">Czech Republic</option>
											<option value="DNK">Denmark</option>
											<option value="DJI">Djibouti</option>
											<option value="DMA">Dominica</option>
											<option value="DOM">Dominican Republic</option>
											<option value="ECU">Ecuador</option>
											<option value="EGY">Egypt</option>
											<option value="SLV">El Salvador</option>
											<option value="GNQ">Equatorial Guinea</option>
											<option value="ERI">Eritrea</option>
											<option value="EST">Estonia</option>
											<option value="ETH">Ethiopia</option>
											<option value="FLK">Falkland Islands (Malvinas)</option>
											<option value="FRO">Faroe Islands</option>
											<option value="FJI">Fiji</option>
											<option value="FIN">Finland</option>
											<option value="FRA">France</option>
											<option value="GUF">French Guiana</option>
											<option value="PYF">French Polynesia</option>
											<option value="ATF">French Southern Territories</option>
											<option value="GAB">Gabon</option>
											<option value="GMB">Gambia</option>
											<option value="GEO">Georgia</option>
											<option value="DEU">Germany</option>
											<option value="GHA">Ghana</option>
											<option value="GIB">Gibraltar</option>
											<option value="GRC">Greece</option>
											<option value="GRL">Greenland</option>
											<option value="GRD">Grenada</option>
											<option value="GLP">Guadeloupe</option>
											<option value="GUM">Guam</option>
											<option value="GTM">Guatemala</option>
											<option value="GGY">Guernsey</option>
											<option value="GIN">Guinea</option>
											<option value="GNB">Guinea-Bissau</option>
											<option value="GUY">Guyana</option>
											<option value="HTI">Haiti</option>
											<option value="HMD">Heard Island and McDonald Islands</option>
											<option value="VAT">Holy See (Vatican City State)</option>
											<option value="HND">Honduras</option>
											<option value="HKG">Hong Kong</option>
											<option value="HUN">Hungary</option>
											<option value="ISL">Iceland</option>
											<option value="IND">India</option>
											<option value="IDN">Indonesia</option>
											<option value="IRN">Iran</option>
											<option value="IRQ">Iraq</option>
											<option value="IRL">Ireland</option>
											<option value="IMN">Isle of Man</option>
											<option value="ISR">Israel</option>
											<option value="ITA">Italy</option>
											<option value="JAM">Jamaica</option>
											<option value="JPN">Japan</option>
											<option value="JEY">Jersey</option>
											<option value="JOR">Jordan</option>
											<option value="KAZ">Kazakhstan</option>
											<option value="KEN">Kenya</option>
											<option value="KIR">Kiribati</option>
											<option value="PRK">Korea</option>
											<option value="KOR">Korea</option>
											<option value="KWT">Kuwait</option>
											<option value="KGZ">Kyrgyzstan</option>
											<option value="LAO">Lao People's Democratic Republic</option>
											<option value="LVA">Latvia</option>
											<option value="LBN">Lebanon</option>
											<option value="LSO">Lesotho</option>
											<option value="LBR">Liberia</option>
											<option value="LBY">Libya</option>
											<option value="LIE">Liechtenstein</option>
											<option value="LTU">Lithuania</option>
											<option value="LUX">Luxembourg</option>
											<option value="MAC">Macao</option>
											<option value="MKD">Macedonia</option>
											<option value="MDG">Madagascar</option>
											<option value="MWI">Malawi</option>
											<option value="MYS">Malaysia</option>
											<option value="MDV">Maldives</option>
											<option value="MLI">Mali</option>
											<option value="MLT">Malta</option>
											<option value="MHL">Marshall Islands</option>
											<option value="MTQ">Martinique</option>
											<option value="MRT">Mauritania</option>
											<option value="MUS">Mauritius</option>
											<option value="MYT">Mayotte</option>
											<option value="MEX">Mexico</option>
											<option value="FSM">Micronesia</option>
											<option value="MDA">Moldova, Republic of</option>
											<option value="MCO">Monaco</option>
											<option value="MNG">Mongolia</option>
											<option value="MNE">Montenegro</option>
											<option value="MSR">Montserrat</option>
											<option value="MAR">Morocco</option>
											<option value="MOZ">Mozambique</option>
											<option value="MMR">Myanmar</option>
											<option value="NAM">Namibia</option>
											<option value="NRU">Nauru</option>
											<option value="NPL">Nepal</option>
											<option value="NLD">Netherlands</option>
											<option value="NCL">New Caledonia</option>
											<option value="NZL">New Zealand</option>
											<option value="NIC">Nicaragua</option>
											<option value="NER">Niger</option>
											<option value="NGA">Nigeria</option>
											<option value="NIU">Niue</option>
											<option value="NFK">Norfolk Island</option>
											<option value="MNP">Northern Mariana Islands</option>
											<option value="NOR">Norway</option>
											<option value="OMN">Oman</option>
											<option value="PAK">Pakistan</option>
											<option value="PLW">Palau</option>
											<option value="PSE">Palestinian Territory, Occupied</option>
											<option value="PAN">Panama</option>
											<option value="PNG">Papua New Guinea</option>
											<option value="PRY">Paraguay</option>
											<option value="PER">Peru</option>
											<option value="PHL">Philippines</option>
											<option value="PCN">Pitcairn</option>
											<option value="POL">Poland</option>
											<option value="PRT">Portugal</option>
											<option value="PRI">Puerto Rico</option>
											<option value="QAT">Qatar</option>
											<option value="REU">Réunion</option>
											<option value="ROU">Romania</option>
											<option value="RUS">Russian Federation</option>
											<option value="RWA">Rwanda</option>
											<option value="BLM">Saint Barthélemy</option>
											<option value="SHN">Saint Helena</option>
											<option value="KNA">Saint Kitts and Nevis</option>
											<option value="LCA">Saint Lucia</option>
											<option value="MAF">Saint Martin (French part)</option>
											<option value="SPM">Saint Pierre and Miquelon</option>
											<option value="VCT">Saint Vincent and the Grenadines</option>
											<option value="WSM">Samoa</option>
											<option value="SMR">San Marino</option>
											<option value="STP">Sao Tome and Principe</option>
											<option value="SAU">Saudi Arabia</option>
											<option value="SEN">Senegal</option>
											<option value="SRB">Serbia</option>
											<option value="SYC">Seychelles</option>
											<option value="SLE">Sierra Leone</option>
											<option value="SGP">Singapore</option>
											<option value="SXM">Sint Maarten (Dutch part)</option>
											<option value="SVK">Slovakia</option>
											<option value="SVN">Slovenia</option>
											<option value="SLB">Solomon Islands</option>
											<option value="SOM">Somalia</option>
											<option value="ZAF">South Africa</option>
											<option value="SGS">South Georgia</option>
											<option value="SSD">South Sudan</option>
											<option value="ESP">Spain</option>
											<option value="LKA">Sri Lanka</option>
											<option value="SDN">Sudan</option>
											<option value="SUR">Suriname</option>
											<option value="SJM">Svalbard and Jan Mayen</option>
											<option value="SWZ">Swaziland</option>
											<option value="SWE">Sweden</option>
											<option value="CHE">Switzerland</option>
											<option value="SYR">Syrian Arab Republic</option>
											<option value="TWN">Taiwan</option>
											<option value="TJK">Tajikistan</option>
											<option value="TZA">Tanzania</option>
											<option value="THA">Thailand</option>
											<option value="TLS">Timor-Leste</option>
											<option value="TGO">Togo</option>
											<option value="TKL">Tokelau</option>
											<option value="TON">Tonga</option>
											<option value="TTO">Trinidad and Tobago</option>
											<option value="TUN">Tunisia</option>
											<option value="TUR">Turkey</option>
											<option value="TKM">Turkmenistan</option>
											<option value="TCA">Turks and Caicos Islands</option>
											<option value="TUV">Tuvalu</option>
											<option value="UGA">Uganda</option>
											<option value="UKR">Ukraine</option>
											<option value="ARE">United Arab Emirates</option>
											<option value="GBR">United Kingdom</option>
											<option value="USA">United States</option>
											<option value="URY">Uruguay</option>
											<option value="UZB">Uzbekistan</option>
											<option value="VUT">Vanuatu</option>
											<option value="VEN">Venezuela</option>
											<option value="VNM">Viet Nam</option>
											<option value="VGB">Virgin Islands, British</option>
											<option value="VIR">Virgin Islands, U.S.</option>
											<option value="WLF">Wallis and Futuna</option>
											<option value="ESH">Western Sahara</option>
											<option value="YEM">Yemen</option>
											<option value="ZMB">Zambia</option>
											<option value="ZWE">Zimbabwe</option>
										</select>
										</div>
										<br>
										
										<label for="location">Location</label>
										<select name="location" id="location" class="form-control">
											<option value="Dhaka">Dhaka</option>
											<option value="Khulna">Khulna</option>
											<option value="Rajshahi">Rajshahi</option>
											<option value="Chittagong">Chittagong</option>
											<option value="Rangpur">Rangpur</option>
											<option value="Sylhet">Sylhet</option>
											<option value="Barisal">Barisal</option>
										</select><br>
										
										<label for="degree">Degree</label>
										<select name="degree" id="degree"  class="form-control">
											<option value="MBA">MBA</option>
											<option value="BBA">BBA</option>
											<option value="HSC">HSC</option>
											<option value="SSC">SSC</option>
											<option value="CA">CA</option>
										</select><br>
										
										<label for="tution">Tution Fee</label>
										<select name="tution" id="tution"  class="form-control">
											<option value="10,000 - 15,000">10,000 - 15,000</option>
											<option value="10,000 - 15,000">15,000 - 20,000</option>
											<option value="10,000 - 15,000">20,000 - 25,000</option>
											<option value="10,000 - 15,000">25,000 - 30,000</option>
											<option value="10,000 - 15,000">30,000 - 35,000</option>
										</select><br>
									</form>
								</div>
							</div>
							
							<div class="col-md-9">
								<div class="right_slider_area">
									<ul class="bxslider">
									  <li><img src="img/banner-university-tours.png" /></li>
									  <li><img src="img/education_banner_what_our_students_say_tcm44-40785.jpg" /></li>
									  <li><img src="img/educationalbanner1.jpg" /></li>
									  <li><img src="img/GI-2015-Prospectus-Web-banner-742x293-V01.jpg" /></li>
									  <li><img src="img/Studentsafe-University-banner.jpg" /></li>
									  <li><img src="img/university_banner.jpg" /></li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</section> <!-- banner area ends here -->
				<section class="content_area">
					<div class="row">
						<div class="col-md-3 pad">
							<div class="events">
								<h4>Events</h4>
								<div class="events_schedule">
									
								</div>
							</div>
						</div>
						<div class="col-md-9">
							<div class="container-fluid">
								<div class="row">
									<div class="col-md-6 col-sm-6">
										<div class="featured">
											<div class="featured_inner">
												<div class="featured_img">
													<img src="images/CA021.jpg" alt="" />
												</div>
												<div class="featured_txt">
													<h4>Santa Ana College </h4>
													<p>Santa Ana College, established in 1915, is one of California's oldest community colleges. Santa Ana ...</p>
												</div>
											</div>
										</div>
									</div>
									<div class="col-md-6 col-sm-6">
										<div class="featured">
											<div class="featured_inner">
												<div class="featured_img">
													<img src="images/CA021.jpg" alt="" />
												</div>
												<div class="featured_txt">
													<h4>Santa Ana College </h4>
													<p>Santa Ana College, established in 1915, is one of California's oldest community colleges. Santa Ana ...</p>
												</div>
											</div>
										</div>
									</div>
									
									<div class="col-md-6 col-sm-6">
										<div class="featured">
											<div class="featured_inner">
												<div class="featured_img">
													<img src="images/CA021.jpg" alt="" />
												</div>
												<div class="featured_txt">
													<h4>Santa Ana College </h4>
													<p>Santa Ana College, established in 1915, is one of California's oldest community colleges. Santa Ana ...</p>
												</div>
											</div>
										</div>
									</div>
									
									<div class="col-md-6 col-sm-6">
										<div class="featured">
											<div class="featured_inner">
												<div class="featured_img">
													<img src="images/CA021.jpg" alt="" />
												</div>
												<div class="featured_txt">
													<h4>Santa Ana College </h4>
													<p>Santa Ana College, established in 1915, is one of California's oldest community colleges. Santa Ana ...</p>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section> <!-- content area ends here -->
			</div>
			<footer class="footer_area">
				<div class="container">
					<div class="row">
						<div class="col-md-3 col-sm-6">
							<div class="footer_about">
								<h2>About us</h2>
								<p>Dlor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor  invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor</p>
							</div>
						</div>
						<div class="col-md-3 col-sm-6">
							<div class="footer_news">
								<div class="online">
									<h2>Who is online?</h2>
									<p>We have 11 guests and no members online</p>
								</div>
								<div class="newsletter">
									<h2>Monthly Newsletter</h2>
									<p>Keep up to date on the latest from our network:</p>
									<form action="#">
										<input type="email" name="email" id="email" placeholder="Email" />
										<input type="submit" value="Enter" />
									</form>
								</div>
							</div>
						</div>
						<div class="col-md-2 col-sm-6">
							<div class="links">
								<h2>Useful Links</h2>
								<ul>
									<li><a href="#">Home</a></li>
									<li><a href="#">Blog</a></li>
									<li><a href="#">Education</a></li>
									<li><a href="#">Contact</a></li>
									<li><a href="#">Forum</a></li>
								</ul>
							</div>
						</div>
						<div class="col-md-4 col-sm-6">
							<div class="contact_now">
								<p><span><a href="#"><i class="fa fa-phone"></i> +880 191 2578522</a></span>support@usastudy.com</p>
								<p>Room 2209 GP Invest Building, 170 La Thanh Street, Dong Da Dist, Ha Noi, Vietnam</p>
							</div>
						</div>
					</div>
					
					<div class="footer_bottom">
						<div class="row">
							<div class="col-md-12">
								<div class="copy">
									<p>Copyright &copy; 2015 US Education | All Rights Reserved. Designed by <a href="http://gaziyeasin.com">Gazi Yeasin</a></p>
								</div>
							</div>
						</div>
					</div>
				</div>	
			</footer>
	   </div>
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.2.min.js"><\/script>')</script>
        <script src="js/plugins.js"></script>
        <script src="js/main.js"></script>
        <script src="js/bootstrap.min.js"></script>
		<script src="js/jquery.bxslider.min.js"></script>
		<script type="text/javascript">
			$(document).ready(function(){
			  $('.bxslider').bxSlider();
			});
			slider = $('.bxslider').bxSlider();
				slider.startAuto();
		</script>


        <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
        <script>
            (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
            function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
            e=o.createElement(i);r=o.getElementsByTagName(i)[0];
            e.src='https://www.google-analytics.com/analytics.js';
            r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
            ga('create','UA-XXXXX-X','auto');ga('send','pageview');
        </script>
    </body>
</html>
