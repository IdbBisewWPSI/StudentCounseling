-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 19, 2015 at 09:31 AM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `projectdb`
--

-- --------------------------------------------------------

--
-- Table structure for table `accommodation`
--

CREATE TABLE IF NOT EXISTS `accommodation` (
`accommodationID` int(11) NOT NULL,
  `universityID` int(11) NOT NULL,
  `degreeID` int(11) NOT NULL,
  `accommodation` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `accommodation`
--

INSERT INTO `accommodation` (`accommodationID`, `universityID`, `degreeID`, `accommodation`) VALUES
(1, 1, 1, 'A unique backstage pass providing an opportunity to stay in historic Oxford college room accommodation these centrally-located bed and breakfast (B&amp;B) rooms start at £30. providing not only a cost effective alternative to Oxford hotels. but also an unforgettable experience.Colleges provide accommodation for all undergraduate students during their first year of study and for at least one other year of their course. Many colleges are also able to provide accommodation to graduate students.As an undergraduate, you can choose to move out of college and live with friends in student accommodation for some of your time studying at Oxford. For graduates. the Graduate Accommodation Office can help if college accommodation is unavailable or not of the type needed.'),
(2, 1, 2, 'Colleges provide accommodation for all undergraduate students during their first year of study and for at least one other year of their course. Many colleges are also able to provide accommodation to graduate students.As an undergraduate, you can choose to move out of college and live with friends in student accommodation for some of your time studying at Oxford. For graduates. the Graduate Accommodation Office can help if college accommodation is unavailable or not of the type needed.Colleges provide accommodation for all undergraduate students during their first year of study and for at least one other year of their course. Many colleges are also able to provide accommodation to graduate students.As an undergraduate, you can choose to move out of college and live with friends in student accommodation for some of your time studying at Oxford. For graduates. the Graduate Accommodation Office can help if college accommodation is unavailable or not of the type needed.'),
(3, 1, 11, 'Whether you are heading to study at The University of Oxford or Oxford Brookes University, you will want to ensure that your accommodation is perfectly situated. Cambridge Terrace is located within the very heart of stunning Oxford, so making the journey to your lectures everyday was not seem like a chore. And if you fancy heading into town afterwards to grab a bite or to meet with friends you won’t have to walk far to reach the hub of the city.'),
(4, 2, 5, 'A unique backstage pass providing an opportunity to stay in historic Oxford college room accommodation these centrally-located bed and breakfast (B&amp;B) rooms start at ï¿½30. providing not only a cost effective alternative to Oxford hotels. but also an unforgettable experience.Colleges provide accommodation for all undergraduate students during their first year of study and for at least one other year of their course. Many colleges are also able to provide accommodation to graduate students.As an undergraduate, you can choose to move out of college and live with friends in student accommodation for some of your time studying at Oxford. For graduates. the Graduate Accommodation Office can help if college accommodation is unavailable or not of the type needed.'),
(5, 2, 8, 'Colleges provide accommodation for all undergraduate students during their first year of study and for at least one other year of their course. Many colleges are also able to provide accommodation to graduate students.As an undergraduate, you can choose to move out of college and live with friends in student accommodation for some of your time studying at Oxford. For graduates. the Graduate Accommodation Office can help if college accommodation is unavailable or not of the type needed.Colleges provide accommodation for all undergraduate students during their first year of study and for at least one other year of their course. Many colleges are also able to provide accommodation to graduate students.As an undergraduate, you can choose to move out of college and live with friends in student accommodation for some of your time studying at Oxford. For graduates. the Graduate Accommodation Office can help if college accommodation is unavailable or not of the type needed.'),
(6, 2, 8, 'Whether you are heading to study at The University of Oxford or Oxford Brookes University, you will want to ensure that your accommodation is perfectly situated. Cambridge Terrace is located within the very heart of stunning Oxford, so making the journey to your lectures everyday was not seem like a chore. And if you fancy heading into town afterwards to grab a bite or to meet with friends you wonï¿½t have to walk far to reach the hub of the city.');

-- --------------------------------------------------------

--
-- Table structure for table `application`
--

CREATE TABLE IF NOT EXISTS `application` (
`applicationid` int(11) NOT NULL,
  `fname` varchar(50) NOT NULL,
  `lname` varchar(50) NOT NULL,
  `countryid` int(11) NOT NULL,
  `locationid` int(11) NOT NULL,
  `state` varchar(50) NOT NULL,
  `postalcode` varchar(50) NOT NULL,
  `postaladdress` text NOT NULL,
  `phone` varchar(50) NOT NULL,
  `mobile` varchar(50) NOT NULL,
  `dob` varchar(30) NOT NULL,
  `gender` varchar(10) NOT NULL,
  `beginstudy` varchar(50) NOT NULL,
  `likedstudy` varchar(50) NOT NULL,
  `tuitionfee` varchar(20) NOT NULL,
  `test` varchar(20) NOT NULL,
  `score` varchar(30) NOT NULL,
  `appdate` varchar(30) NOT NULL,
  `comments` text NOT NULL,
  `email` varchar(200) NOT NULL,
  `status` varchar(20) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `application`
--

INSERT INTO `application` (`applicationid`, `fname`, `lname`, `countryid`, `locationid`, `state`, `postalcode`, `postaladdress`, `phone`, `mobile`, `dob`, `gender`, `beginstudy`, `likedstudy`, `tuitionfee`, `test`, `score`, `appdate`, `comments`, `email`, `status`) VALUES
(1, 'Hafizur', 'Rahman', 0, 0, 'Dhaka', '1205', 'Dhanmondi', '6366546', '464646', '2015-04-15', '1', '2015-04-17', 'hafiz.ubikm@gmail.com', '10,000 - 15,000', 'ITEP', '5', '2015-04-15', 'sfsad sdfas fas asfas fasfa asfafa fasfas', 'hafiz.ubikm@gmail.com', 'Progress'),
(7, 'Delware', 'Sumon', 0, 0, 'Dhaka', '1205', 'Dhanmondi', '6366546', '01926743629', '2015-04-22', 'Male', '2015-04-20', 'hafiz.ubikm@gmail.com', '10,000 - 15,000', 'Pearson', '5', '2015-04-21', 'Other Comments or Questions in English', 'hafiz.ubikm@gmail.com', 'Active');

-- --------------------------------------------------------

--
-- Table structure for table `apprequirements`
--

CREATE TABLE IF NOT EXISTS `apprequirements` (
`appRequirementsID` int(11) NOT NULL,
  `universityID` int(11) NOT NULL,
  `degreeID` int(11) NOT NULL,
  `appRequirements` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `city`
--

CREATE TABLE IF NOT EXISTS `city` (
`cityid` int(11) NOT NULL,
  `countryid` int(11) NOT NULL,
  `cityname` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `city`
--

INSERT INTO `city` (`cityid`, `countryid`, `cityname`) VALUES
(1, 1, 'Oxford OX1 2JD'),
(2, 2, 'Cambridge'),
(3, 3, 'Sydney'),
(4, 3, 'Canberra'),
(5, 4, 'Singapore');

-- --------------------------------------------------------

--
-- Table structure for table `country`
--

CREATE TABLE IF NOT EXISTS `country` (
`countryid` int(11) NOT NULL,
  `countryname` varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `country`
--

INSERT INTO `country` (`countryid`, `countryname`) VALUES
(1, 'United Kingdom'),
(2, 'United State'),
(3, 'Australia'),
(4, 'Singapore');

-- --------------------------------------------------------

--
-- Table structure for table `degree`
--

CREATE TABLE IF NOT EXISTS `degree` (
`degreeid` int(11) NOT NULL,
  `degree` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `degree`
--

INSERT INTO `degree` (`degreeid`, `degree`) VALUES
(1, 'Doctor of Philosophy (DPhil)'),
(2, 'MSc by Research'),
(3, 'Master of Letters (MLitt)'),
(4, 'Master of Philosophy (MPhil)'),
(5, 'Master of Studies (MSt)'),
(6, 'Master of Science (MSc) by coursework'),
(7, 'Bachelor of Civil Law (BCL)'),
(8, 'Magister Juris (MJur)'),
(9, 'Master of Business Administration (MBA)'),
(10, 'Executive MBA (EMBA)'),
(11, 'Master of Fine Art (MFA)'),
(12, 'Master of Public Policy (MPP)'),
(13, 'Master of Theology (MTh)');

-- --------------------------------------------------------

--
-- Table structure for table `degreecontent`
--

CREATE TABLE IF NOT EXISTS `degreecontent` (
`slno` int(11) NOT NULL,
  `universityID` int(11) NOT NULL,
  `degreeID` int(11) NOT NULL,
  `degreeContent` text NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `degreecontent`
--

INSERT INTO `degreecontent` (`slno`, `universityID`, `degreeID`, `degreeContent`) VALUES
(1, 44, 5, 'The University of Minnesota has been offering English as a Second Language courses and programs since 1968. Since then, we have helped thousands of students from across the globe achieve academic success, advance their careers, and develop international networks of friends.'),
(2, 44, 9, 'The University of Minnesota has been offering English as a Second Language courses and programs since 1968. Since then, we have helped .'),
(3, 45, 10, 'The mysqli_num_rows() function returns the number of rows in a The mysqli_num_rows() function returns the number of rows in a result set. result set. The mysqli_num_rows() function returns the number of rows in a result set. The University of Minnesota has been offering English as a Second Language courses and programs since 1968. Since then, we have helped thousands of students from across the globe achieve academic success, advance their careers, and develop international networks of friends.'),
(4, 45, 10, 'The University of Minnesota has been offering English as a Second Language courses and programs since 1968. Since then, we have helped thousands of students from across the globThe University of Minnesota has been offering English as a Second Language courses and programs since 1968. Since then, we have helped thousands of students from across the globe achieve academic success, advance their careers, and develop international networks of friends.');

-- --------------------------------------------------------


--

CREATE TABLE IF NOT EXISTS `jobopportunities` (
`jobOpportunitiesID` int(11) NOT NULL,
  `universityID` int(11) NOT NULL,
  `degreeID` int(11) NOT NULL,
  `jobOpportunities` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jobopportunities`
--

INSERT INTO `jobopportunities` (`jobOpportunitiesID`, `universityID`, `degreeID`, `jobOpportunities`) VALUES
(1, 1, 1, '&nbsp;<h4>Sales Account Manager</h4><br>Salary $25,513 plus OTE up to $7k plus $1k discretionary payment<br>Full time, permanent'),
(2, 1, 11, '<h4>Dining Room Assistant</h4><br>Grade: $14,959 - $15,765 (discretionary range to $16,131)<br>Fixed-Term, Maternity Cover<br>Apply by: 20 May 2015<br>We are seeking to recruit a Dining Room Assistant to cover a period of maternity <br>leave in the dining and catering department based at Egrove Park, Kennington.'),
(3, 2, 5, 'Sales Account Manager<br><br>Salary $25,513 plus OTE up to $7k plus $1k discretionary <br><br>payment<br>Full time, permanent'),
(4, 2, 11, 'Dining Room Assistant<br><br>Grade: $14,959 - $15,765 (discretionary range to $16,131)<br>Fixed-Term, Maternity Cover<br>Apply by: 20 May 2015<br>We are seeking to recruit a Dining Room Assistant to cover <br><br>a period of maternity<br>leave in the dining and catering department based at Egrove <br><br>Park, Kennington.');

-- --------------------------------------------------------

--
-- Table structure for table `message`
--

CREATE TABLE IF NOT EXISTS `message` (
`id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `email` varchar(100) NOT NULL,
  `message` text NOT NULL,
  `txtdate` varchar(50) NOT NULL,
  `university` varchar(200) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `message`
--

INSERT INTO `message` (`id`, `name`, `email`, `message`, `txtdate`, `university`) VALUES
(5, 'hafiz', 'hafiz.ubikm@gmail.com', 'Bangladesh. Established during the British Raj in 1921, it gained a reputation as the "Oxford of the East" during its early years and has been a significant contributor to the modern history of Bangladesh.', '08/03/2015', 'Dhaka University'),
(6, 'Sumon', 'sumon@gmal.com', 'Bangladesh. Established during the British Raj in 1921, it gained a reputation as the "Oxford of the East" during its early years and has been a significant contributor to the modern history of Bangladesh.', '08/03/2015', 'Dhaka University'),
(7, 'manna', 'Manna@gmai.com', 'Bangladesh. Established during the British Raj in 1921, it gained a reputation as the "Oxford of the East" during its early years and has been a significant contributor to the modern history of Bangladesh.', '08/03/2015', 'Dhaka University'),
(8, 'Ashik', 'ashik@gmail.com', 'Bangladesh. Established during the British Raj in 1921, it gained a reputation as the "Oxford of the East" during its early years and has been a significant contributor to the modern history of Bangladesh.', '08/03/2015', 'Dhaka University'),
(9, 'Mamun', 'manun@gmail.com', 'Bangladesh. Established during the British Raj in 1921, it gained a reputation as the "Oxford of the East" during its early years and has been a significant contributor to the modern history of Bangladesh.', '08/03/2015', 'Dhaka University'),
(10, 'Joy', 'joy@gmail.com', 'Bangladesh. Established during the British Raj in 1921, it gained a reputation as the "Oxford of the East" during its early years and has been a significant contributor to the modern history of Bangladesh.', '08/03/2015', 'Dhaka University'),
(11, 'Delware Sumon', 'delware@gmail.com', 'Hello I want to learn in this ', '10/03/2015', 'University of Oxford'),
(12, 'Delware Sumon', 'delware@gmail.com', 'Hello I want to learn in this ', '10/03/2015', 'University of Oxford');

-- --------------------------------------------------------

--
-- Table structure for table `researchassistances`
--

CREATE TABLE IF NOT EXISTS `researchassistances` (
`researchassistancesID` int(11) NOT NULL,
  `universityID` int(11) NOT NULL,
  `degreeID` int(11) NOT NULL,
  `ResearchAssistances` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `scholarshipfee`
--

CREATE TABLE IF NOT EXISTS `scholarshipfee` (
`scholarshipFeeID` int(11) NOT NULL,
  `universityID` int(11) NOT NULL,
  `degreeID` int(11) NOT NULL,
  `scholarshipFee` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `subject`
--

CREATE TABLE IF NOT EXISTS `subject` (
`subjectid` int(11) NOT NULL,
  `subjectname` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subject`
--

INSERT INTO `subject` (`subjectid`, `subjectname`) VALUES
(1, 'African Studies'),
(2, 'Ancient History'),
(3, 'Applied Statistics'),
(4, 'Islamic Studies and History'),
(5, 'Health Research'),
(6, 'Foundations of Computer Science'),
(7, 'Computer Science'),
(8, 'Major Programme Management');

-- --------------------------------------------------------

--
-- Table structure for table `tutionfee`
--

CREATE TABLE IF NOT EXISTS `tutionfee` (
`tuitionFeeID` int(11) NOT NULL,
  `universityID` int(11) NOT NULL,
  `degreeID` int(11) NOT NULL,
  `tuitionFees` text NOT NULL,
  `livingcost` varchar(10) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tutionfee`
--

INSERT INTO `tutionfee` (`tuitionFeeID`, `universityID`, `degreeID`, `tuitionFees`, `livingcost`) VALUES
(1, 1, 1, 'undergraduate-level programs: USA$9,000 per year', '$5000'),
(2, 1, 11, 'over US$25,000 (US$42000) per year', '$46463'),
(3, 1, 4, 'over US$25,000 (US$42,000) per year', '$67645'),
(4, 1, 11, 'undergraduate-level programs: US$90000 per year', '$6892'),
(5, 2, 5, 'undergraduate-level programs: USA$9,000 per year', '$5000'),
(6, 2, 8, 'over US$25,000 (US$42000) per year', ' 	$46463'),
(7, 2, 11, 'over US$25,000 (US$42,000) per year', '$67645'),
(8, 2, 2, 'undergraduate-level programs: US$90000 per year', ' 	$6892');

-- --------------------------------------------------------

--
-- Table structure for table `tutionfeerange`
--

CREATE TABLE IF NOT EXISTS `tutionfeerange` (
`id` int(11) NOT NULL,
  `tutionfeerange` varchar(30) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tutionfeerange`
--

INSERT INTO `tutionfeerange` (`id`, `tutionfeerange`) VALUES
(1, '$25000-$50000'),
(2, '$9000-$1500000'),
(3, '$14845-$21855'),
(4, '$9000-$15000');

-- --------------------------------------------------------

--
-- Table structure for table `university`
--

CREATE TABLE IF NOT EXISTS `university` (
`universityid` int(11) NOT NULL,
  `logo` varchar(100) NOT NULL,
  `universityname` varchar(100) NOT NULL,
  `countryid` int(11) NOT NULL,
  `cityid` int(11) NOT NULL,
  `aboutuniversity` text,
  `applicationdeadline` date DEFAULT NULL,
  `applicationperiod` varchar(11) DEFAULT NULL,
  `email` varchar(100) DEFAULT NULL,
  `web` varchar(200) DEFAULT NULL,
  `fontpage` varchar(200) DEFAULT NULL,
  `rank` int(15) DEFAULT NULL,
  `rankimage` varchar(200) DEFAULT NULL,
  `mapurl` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `university`
--

INSERT INTO `university` (`universityid`, `logo`, `universityname`, `countryid`, `cityid`, `aboutuniversity`, `applicationdeadline`, `applicationperiod`, `email`, `web`, `fontpage`, `rank`, `rankimage`, `mapurl`) VALUES
(1, '0513201505211203102015202647oxfordlogo.png', 'University of Oxford', 1, 1, 'The University of Oxford (informally Oxford University or simply Oxford) is a collegiate research university located in Oxford, England. While having no known date of foundation, there is evidence of teaching as far back as 1096, making it the oldest university in the English-speaking world, and the world''s second-oldest surviving university.It grew rapidly from 1167 when Henry II banned English students from attending the University of Paris. After disputes between students and Oxford townsfolk in 1209, some academics fled northeast to Cambridge, where they established what became the University of Cambridge.The two "ancient universities" are frequently jointly referred to as "Oxbridge".', '2015-10-10', '6months', 'oxford@gmail.com', 'www.ox.ac.uk', '0513201505211203102015202647campus02.jpg', 8, '3', ''),
(2, '0514201505080005132015223751Harvard_Logo.jpg', 'Harvard University', 2, 2, 'Harvard University is a private Ivy League research university in Cambridge, Massachusetts, established in 1636. Its history, influence and wealth have made it one of the most prestigious universities in the world.Established originally by the Massachusetts legislature and soon thereafter named for John Harvard (its first benefactor), Harvard is the United States'''' oldest institution of higher learning,and the Harvard Corporation (formally, the President and Fellows of Harvard College) is its first chartered corpation.Although never formally affiliated with any denomination, the early College primarily trained CongregationÂ­alist and Unitarian clergy. Its curriculum and student body were gradually secularized during the 18th century, and by the 19th century Harvard had emerged as the central cultural establishment among Boston elites.', '2015-12-12', '6months', 'info_center@harvard.edu', 'www.harvard.edu', 'Havard.jpg', 8, '5', '');

-- --------------------------------------------------------

--
-- Table structure for table `universitycampus`
--

CREATE TABLE IF NOT EXISTS `universitycampus` (
`slno` int(11) NOT NULL,
  `universityid` int(11) NOT NULL,
  `imagetitle` varchar(200) NOT NULL,
  `campusimage` varchar(50) NOT NULL,
  `campuslocation` varchar(200) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `universitycampus`
--

INSERT INTO `universitycampus` (`slno`, `universityid`, `imagetitle`, `campusimage`, `campuslocation`) VALUES
(1, 1, 'Headington Campus', 'Headington.jpg', ' Headington Hill, and Marston Road sites.'),
(2, 1, 'Wheatley Campus', 'Wheatley.jpg', 'Wheatley, OX33, 1HX'),
(3, 1, 'Harcourt Hill Campus', 'camp1.jpg', 'Headington Hill, Oxford, OX3 0BP'),
(4, 1, 'Swindon Campus', 'Swindon.jpg', 'Victorian building on Ferndale Road, Swindon.'),
(5, 2, 'Harvard Extension School ', 'Haverd_extension school.jpg', ''),
(6, 2, 'Harvard College', 'haverd_college.jpg', ''),
(7, 2, ' Harvard Medical school', 'havard_medical_school.jpg', ''),
(8, 2, 'Harvard school of Public health', 'school_of_public_health.jpg', '');

-- --------------------------------------------------------

--
-- Table structure for table `universitydegree`
--

CREATE TABLE IF NOT EXISTS `universitydegree` (
`slno` int(11) NOT NULL,
  `univercityid` int(11) NOT NULL,
  `degreeid` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `universitydegree`
--

INSERT INTO `universitydegree` (`slno`, `univercityid`, `degreeid`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 1, 11),
(4, 1, 5),
(5, 1, 4),
(6, 2, 5),
(7, 2, 8),
(8, 2, 11),
(9, 2, 2);

-- --------------------------------------------------------

--
-- Table structure for table `universitysubject`
--

CREATE TABLE IF NOT EXISTS `universitysubject` (
`slno` int(11) NOT NULL,
  `univercityid` int(11) NOT NULL,
  `subjectid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
`id` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `password`) VALUES
(8, 'hafiz', '5word'),
(9, 'sumon', '123456'),
(10, 'fouzia', '123456'),
(11, 'monto', 'monto');

-- --------------------------------------------------------

--
-- Structure for view `displaycity`
--


-- --------------------------------------------------------

--
-- Structure for view `distutionfee`
--


--
-- Indexes for dumped tables
--

--
-- Indexes for table `accommodation`
--
ALTER TABLE `accommodation`
 ADD PRIMARY KEY (`accommodationID`);

--
-- Indexes for table `application`
--
ALTER TABLE `application`
 ADD PRIMARY KEY (`applicationid`);

--
-- Indexes for table `apprequirements`
--
ALTER TABLE `apprequirements`
 ADD PRIMARY KEY (`appRequirementsID`);

--
-- Indexes for table `city`
--
ALTER TABLE `city`
 ADD PRIMARY KEY (`cityid`);

--
-- Indexes for table `country`
--
ALTER TABLE `country`
 ADD PRIMARY KEY (`countryid`);

--
-- Indexes for table `degree`
--
ALTER TABLE `degree`
 ADD PRIMARY KEY (`degreeid`);

--
-- Indexes for table `degreecontent`
--
ALTER TABLE `degreecontent`
 ADD PRIMARY KEY (`slno`);

--
-- Indexes for table `jobopportunities`
--
ALTER TABLE `jobopportunities`
 ADD PRIMARY KEY (`jobOpportunitiesID`);

--
-- Indexes for table `message`
--
ALTER TABLE `message`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `researchassistances`
--
ALTER TABLE `researchassistances`
 ADD PRIMARY KEY (`researchassistancesID`);

--
-- Indexes for table `scholarshipfee`
--
ALTER TABLE `scholarshipfee`
 ADD PRIMARY KEY (`scholarshipFeeID`);

--
-- Indexes for table `subject`
--
ALTER TABLE `subject`
 ADD PRIMARY KEY (`subjectid`);

--
-- Indexes for table `tutionfee`
--
ALTER TABLE `tutionfee`
 ADD PRIMARY KEY (`tuitionFeeID`);

--
-- Indexes for table `tutionfeerange`
--
ALTER TABLE `tutionfeerange`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `university`
--
ALTER TABLE `university`
 ADD PRIMARY KEY (`universityid`);

--
-- Indexes for table `universitycampus`
--
ALTER TABLE `universitycampus`
 ADD PRIMARY KEY (`slno`);

--
-- Indexes for table `universitydegree`
--
ALTER TABLE `universitydegree`
 ADD PRIMARY KEY (`slno`);

--
-- Indexes for table `universitysubject`
--
ALTER TABLE `universitysubject`
 ADD PRIMARY KEY (`slno`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `accommodation`
--
ALTER TABLE `accommodation`
MODIFY `accommodationID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `application`
--
ALTER TABLE `application`
MODIFY `applicationid` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `apprequirements`
--
ALTER TABLE `apprequirements`
MODIFY `appRequirementsID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `city`
--
ALTER TABLE `city`
MODIFY `cityid` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `country`
--
ALTER TABLE `country`
MODIFY `countryid` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `degree`
--
ALTER TABLE `degree`
MODIFY `degreeid` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `degreecontent`
--
ALTER TABLE `degreecontent`
MODIFY `slno` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `jobopportunities`
--
ALTER TABLE `jobopportunities`
MODIFY `jobOpportunitiesID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `message`
--
ALTER TABLE `message`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `researchassistances`
--
ALTER TABLE `researchassistances`
MODIFY `researchassistancesID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `scholarshipfee`
--
ALTER TABLE `scholarshipfee`
MODIFY `scholarshipFeeID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `subject`
--
ALTER TABLE `subject`
MODIFY `subjectid` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `tutionfee`
--
ALTER TABLE `tutionfee`
MODIFY `tuitionFeeID` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `tutionfeerange`
--
ALTER TABLE `tutionfeerange`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `university`
--
ALTER TABLE `university`
MODIFY `universityid` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `universitycampus`
--
ALTER TABLE `universitycampus`
MODIFY `slno` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `universitydegree`
--
ALTER TABLE `universitydegree`
MODIFY `slno` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `universitysubject`
--
ALTER TABLE `universitysubject`
MODIFY `slno` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
