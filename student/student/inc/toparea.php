				<section class="banner_area">
					<div class="left_form_area">
						<div class="row">
							<div class="col-md-3">
								<div class="form">
									<form action="#" method="post">
										<div class="select_country">
											<label for="country">Country</label>
										  <select name="country" class="form-control " id="country">
										   <option value=""  >Select Your Country</option>
											<?php
												$countryquery="select * from country";
												$countryrst=$mysqli->query($countryquery);
												while($courtryrow=$countryrst->fetch_row()){
											?>
											<option value="<?php echo $courtryrow[1];?>"><?php echo $courtryrow[1];?></option>
											<?php }	?>
										  </select>
										</div>
							
										
										<label for="location">Location</label>
										<select name="location" id="location" class="form-control">
											<option value="">Select Your Location</option>
										</select>
										
										<label for="degree">Degree</label>
										<select name="degree" id="degree"  class="form-control">
											<option value=""  >Select Your Degree</option>
											
										</select>
										 
										<label for="tution">Tution Fee</label>
										  <select name="feerange" class="form-control " id="feerange">
										   <option value=""  >Select Your Fee Range</option>
											<?php
												$tutionfeerange="select * from tutionfeerange";
												$ranges=$mysqli->query($tutionfeerange);
												while($rnage=$ranges->fetch_row()){
											?>
											<option value="<?php echo $rnage[1];?>"><?php echo $rnage[1];?></option>
											<?php }	?>
										  </select><br>
										<input type="submit" name="submitquery" value="Search University" class="btn btn-success form-control"/>
									</form>
								</div>
							</div>
							
							<div class="col-md-9">
								<div class="right_slider_area">
									<ul class="bxslider">
											<?php 
											$q="select * from university ORDER BY universityid DESC LIMIT 5";
											$rst=$mysqli->query($q);
											   while($universitty=$rst->fetch_row()){
											?>								
									  <li><img src="admin/images/<?php echo $universitty[10]?>" title="<?php echo $universitty[2]?>" /></li>
										<?php }?>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</section> <!-- banner area ends here -->