			<footer class="footer_area">
				<div class="container">
					<div class="row">
						<div class="col-md-3 col-sm-6">
							<div class="footer_about">
								<h2>About us</h2>
								<p>Dlor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor  invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor</p>
							</div>
						</div>
						<div class="col-md-3 col-sm-6">
							<div class="footer_news">
								<div class="online">
									<h2>Who is online?</h2>
									<p>We have 11 guests and no members online</p>
								</div>
								<div class="newsletter">
									<h2>Monthly Newsletter</h2>
									<p>Keep up to date on the latest from our network:</p>
									<form action="#">
										<input type="email" name="email" id="email" placeholder="Email" />
										<input type="submit" value="Enter" />
									</form>
								</div>
							</div>
						</div>
						<div class="col-md-2 col-sm-6">
							<div class="links">
								<h2>Useful Links</h2>
								<ul>
									<li><a href="#">Home</a></li>
									<li><a href="#">Blog</a></li>
									<li><a href="#">Education</a></li>
									<li><a href="#">Contact</a></li>
									<li><a href="#">Forum</a></li>
								</ul>
							</div>
						</div>
						<div class="col-md-4 col-sm-6">
							<div class="contact_now">
								<p><span><a href="#"><i class="fa fa-phone"></i> +880 123 4567890</a></span>support@usastudy.com</p>
								<p>Room 2209 GP Invest Building, 170 La Thanh Street, Dong Da Dist, Ha Noi, Vietnam</p>
							</div>
						</div>
					</div>
					
					<div class="footer_bottom">
						<div class="row">
							<div class="col-md-12">
								<div class="copy">
									<p>Copyright &copy; 2015 US Education | All Rights Reserved.</p>
								</div>
							</div>
						</div>
					</div>
				</div>	
			</footer>
	   </div><!-- .main-container area ends here -->
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="app/js/vendor/jquery-1.11.2.min.js"><\/script>')</script>
        <script src="app/js/plugins.js"></script>
        <script src="app/js/main.js"></script>
        <script src="app/js/bootstrap.min.js"></script>
		<script src="app/js/jquery.bxslider.min.js"></script>
		<script type="text/javascript">
			$(document).ready(function(){
			  $('.bxslider').bxSlider({
				captions: true
			  });
			});
			slider = $('.bxslider').bxSlider();
			slider.startAuto();
		</script>


        <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
        <script>
            (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
            function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
            e=o.createElement(i);r=o.getElementsByTagName(i)[0];
            e.src='https://www.google-analytics.com/analytics.js';
            r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
            ga('create','UA-XXXXX-X','auto');ga('send','pageview');
        </script>
    </body>
</html>