<?php include('dbcon.php');?>
<!DOCTYPE html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="x-ua-compatible" content="ie=edge">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="apple-touch-icon" href="app/apple-touch-icon.png">
        <!-- Place favicon.ico in the root directory -->
        <link rel="stylesheet" href="app/css/normalize.css">
        <link rel="stylesheet" href="app/css/main.css">
        <link rel="stylesheet" href="app/css/bootstrap.min.css">
        <link rel="stylesheet" href="app/css/font-awesome.min.css">
		<link rel="stylesheet" href="app/css/jquery.bxslider.css"/>
        <link rel="stylesheet" href="app/style.css">
        <link rel="stylesheet" href="app/css/responsive.css">
		<script src="app/js/vendor/jquery-1.11.2.min.js"></script>
		<script src="app/js/dropdown.js"></script>
        <script src="app/js/vendor/modernizr-2.8.3.min.js"></script>
    </head>
    <body>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

       <div class="main-container">
			<div class="container">
				<header class="header_area">
					<div class="header_inner">
						<div class="row">
							<div class="col-md-3">
								<div class="social">
									<ul>
										<li><a href="#"><i class="fa fa-facebook"></i></a></li>
										<li><a href="#"><i class="fa fa-twitter"></i></a></li>
										<li><a href="#"><i class="fa fa-pinterest-p"></i></a></li>
										<li><a href="#"><i class="fa fa-google-plus"></i></a></li>
										<li><a href="#"><i class="fa fa-dribbble"></i></a></li>
									</ul>
								</div>
							</div>
							<div class="col-md-9 mail">
								<div class="mail_area">
									<ul>
										<li><a href="mailto:#"><i class="fa fa-envelope-o"></i> abc@gmail.com</a></li>
										<li><a href="#"><i class="fa fa-phone"></i> +880 123 4567890</a></li>
										<li><a href="admin/index.php"><i class="fa fa-user-plus"></i></i> admin</a></li>
									</ul>
								</div>
							</div>
						</div>