<?php session_start(); $_SESSION['universityidd'];?>
<?php include_once('inc/header.php')?>
<?php include_once('inc/menuforuniversity.php')?>		
<?php include_once('inc/leftmenu.php')?>
<?php
if(isset($_POST['submitapp'])){
	$query="insert  into application(fname,lname,countryid,locationid,state,postalcode,postaladdress,phone,mobile,dob,gender,beginstudy,likedstudy,tuitionfee,test,score,appdate,comments,email,status) values('".$_POST['fname']."','".$_POST['lname']."','".$_POST['country']."','".$_POST['location']."','".$_POST['state']."','".$_POST['postalcode']."','".$_POST['postaladdress']."','".$_POST['phone']."','".$_POST['mobile']."','".$_POST['dob']."','".$_POST['gender']."','".$_POST['beginstudy']."','".$_POST['likedstudy']."','".$_POST['tuitionfee']."','".$_POST['test']."','".$_POST['score']."','".$_POST['appdate']."','".$_POST['comments']."','".$_POST['email']."','Progress')";
	$rstt=$mysqli->query($query);
}
?>
						<div class="col-md-6 col-xs-12 col-sm-8">
							<div class="breadcram">
								<p style="font-size:20px; font-weight:bold">Program Contents</p>
							</div>
							
							<div class="content_area">
								<div class="content">
										<?php
											if(isset($_GET['degreeid'])){
											$query="select * from degreecontent where degreeID='".$_GET['degreeid']."' and universityID='".$_SESSION['universityidd']."'";
											$rst=$mysqli->query($query);
											$row=$rst->fetch_row();
											echo "<h2>Study in this university </h2>";
											echo "<p>".$row[3]."</p>";
											}else{
												echo "<h2>Select Your Program To see Descriptions.</h2>";
												echo "<p>The University of Minnesota has been offering English as a Second Language courses and programs since 1968. Since then, we have helped thousands of students from across the globe achieve academic success, advance their careers, and develop international networks of friends.</p>";
											}
										?>
								</div>
							</div>
						</div>
						<div class="col-md-3 pad col-xs-12 col-sm-12">
							<div class="apply">
								<div class="apply_inner">
									<h2>Apply Now</h2>
									<div class="apply_form">
										<form action="#" method="post">
											<p style="color:red"><b>Please complete this form in English.<br> (*=Required field.)</b> </p>
											 <div class="form-group">
												<label for="firstname">First name<span class="star">*</span></label>
												<input type="text" class="form-control" name="fname" id="name" placeholder="First Name" required>
											  </div>
											 <div class="form-group">
												<label for="lastname">Last name<span class="star">*</span></label>
												<input type="text" class="form-control" name="lname" id="laname" placeholder="Last Name" required>
											  </div>
											 <div class="form-group">
												<label for="country">Country<span class="star">*</span></label>
												  <select name="country" class="form-control " id="country" required>
												   <option value=""  >Select Your Country</option>
													<?php
														$countryquery="select * from country";
														$countryrst=$mysqli->query($countryquery);
														while($courtryrow=$countryrst->fetch_row()){
													?>
													<option value="<?php echo $courtryrow[1];?>"><?php echo $courtryrow[1];?></option>
													<?php }	?>
												  </select>
											  </div>
											 <div class="form-group">
												<label for="location">Location</label>
												<select name="location" id="location" class="form-control">
													<option value="">Select Your Location</option>
												</select>
											  </div>
											 <div class="form-group">
												<label for="state">State/province</label>
												<input type="text" class="form-control" name="state" id="state" placeholder="State">
											  </div>
											 <div class="form-group">
												<label for="postal">Postal code</label>
												<input type="text" class="form-control" id="postalcode" name="postalcode" placeholder="Postal Code">
											  </div>
											 <div class="form-group">
												<label for="postaladd">Postal address</label>
												<input type="text" class="form-control" name="postaladdress" id="postal" placeholder="Postal Address">
											  </div>
											 <div class="form-group">
												<label for="telephone">Telephone</label>
												<input type="text" class="form-control" name="phone" id="phone" placeholder="Telephone">
											  </div>
											 <div class="form-group">
												<label for="mobile">Mobile Number</label>
												<input type="text" class="form-control" name="mobile" id="mobile" placeholder="Mobile Number">
											  </div>
											 <div class="form-group">
												<label for="birth">Date of birth</label>
												<input type="date" class="form-control" name="dob" id="mobile" placeholder="Date of birth">
											  </div>
											 <div class="form-group">
												<label for="gender">Gender<span class="star">*</span></label>
												<input type="radio" id="gender" name="gender" value="Male"  required> Male 
												<input type="radio" id="gender" name="gender" value="Female"> Female <br>
											  </div>
											 <div class="form-group">
												<label for="time">When do you plan to begin your program of study? </label>
												<input type="date" class="form-control" name="beginstudy" id="time" placeholder="">
											  </div>
											 <div class="form-group">
												<label for="like">What would you like to study?<span class="star">*</span>  </label>
												<input type="text" class="form-control" id="like" name="likedstudy" placeholder="" required>
											  </div>
											 <div class="form-group">
												<label for="pay">How much can you afford to pay per year for tuition? <span class="star" required>*</span></label>
												<select id="pay" name="tuitionfee"  class="form-control">
													<option value="10,000 - 15,000">10,000 - 15,000</option>
													<option value="10,000 - 15,000">15,000 - 20,000</option>
													<option value="10,000 - 15,000">20,000 - 25,000</option>
													<option value="10,000 - 15,000">25,000 - 30,000</option>
													<option value="10,000 - 15,000">30,000 - 35,000</option>
												</select>
											  </div>
											 <div class="form-group">
												<label for="test">If Any Test ?</label>
												<select name="test" id="pay"  class="form-control">
													<option value="ACT">ACT</option>
													<option value="GMAT">GMAT</option>
													<option value="GRE">GRE</option>
													<option value="IELTS">IELTS</option>
													<option value="ITEP">ITEP</option>
													<option value="Pearson">Pearson</option>
													<option value="SAT">SAT</option>
													<option value="TOFEL">TOFEL</option>
												</select>
											  </div>
											 <div class="form-group">
												<label for="score">Score</label>
												<input type="text" class="form-control" name="score" id="score" placeholder="">
											  </div>
											  
											 <div class="form-group">
												<label for="datetaken">Date Taken</label>
												<input type="date" class="form-control" id="datetaken" name="appdate" placeholder="">
											  </div>

											 <div class="form-group">
												<label for="comments">Other Comments or Questions (in English) </label>
												<textarea class="form-control" rows="3" name="comments"></textarea>
											  </div>
												<div class="form-group">
													<label for="email1">Email address<span class="star">*</span></label>
													<input type="email" class="form-control" id="Email1" name="email" placeholder="Enter email" required>
												 </div>
												<div class="form-group">
													<label for="emailcon">Conform email address<span class="star">*</span></label>
													<input type="email" class="form-control" id="Email2" name="likedstudy" placeholder="Conform email" required>
												 </div>
												 <input class="btn btn-default" type="submit" name="submitapp" value="Submit"/>
										</form>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
			</div>
<?php include_once('inc/footer.php')?>